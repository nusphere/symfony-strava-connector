<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Tests\Service;

use NuBox\Strava\Api\DataObject\Activity;
use NuBox\Strava\Api\DataObject\StravaTokenInterface;
use NuBox\Strava\Api\Service\StravaApi;
use NuBox\Strava\Api\Tests\TestingKernel;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class ActivityTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testFetchActivity($mockResponse): void
    {
        $kernel = new TestingKernel();

        $httpClientMock = $this->createMock(HttpClientInterface::class);
        $responseMock = $this->createMock(ResponseInterface::class);
        $token = $this->createMock(StravaTokenInterface::class);

        $kernel->boot();
        $container = $kernel->getContainer();

        $stravaAPI = $container->get(StravaApi::class);
        self::assertInstanceOf(StravaApi::class, $stravaAPI);

        $stravaAPI->setClient($httpClientMock);

        $token
            ->expects(self::once())
            ->method('isAccessTokenValid')
            ->willReturn(true);

        $responseMock
            ->expects(self::once())
            ->method('getContent')
            ->willReturn($mockResponse);

        $httpClientMock
            ->expects(self::once())
            ->method('request')
            ->willReturn($responseMock);

        $activity = $stravaAPI->getActivity($token, 99999);

        self::assertInstanceOf(Activity::class, $activity);
    }

    public function dataProvider(): array
    {
        return [
            'default' => [file_get_contents(dirname(__DIR__) . '/_Mock/activity.json')],
            'manual' => [file_get_contents(dirname(__DIR__) . '/_Mock/activity_2.json')],
            'short' => [file_get_contents(dirname(__DIR__) . '/_Mock/activity_3.json')],
            'badminton' => [file_get_contents(dirname(__DIR__) . '/_Mock/activity_badminton.json')],
            'tennis' => [file_get_contents(dirname(__DIR__) . '/_Mock/activity_tennis.json')],
            'only_followers' => [file_get_contents(dirname(__DIR__) . '/_Mock/activity_only_followers.json')],
        ];
    }
}
