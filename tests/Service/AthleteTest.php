<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Tests\Service;

use NuBox\Strava\Api\DataObject\Athlete;
use NuBox\Strava\Api\DataObject\StravaToken;
use NuBox\Strava\Api\DataObject\StravaTokenInterface;
use NuBox\Strava\Api\Service\StravaApi;
use NuBox\Strava\Api\Tests\NuBoxTestingKernel;
use NuBox\Strava\Api\Tests\TestingKernel;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class AthleteTest extends TestCase
{
    public function testFetchAthlete(): void
    {
        $kernel = new TestingKernel();
        $kernel->boot();
        $container = $kernel->getContainer();

        $httpClientMock = $this->createMock(HttpClientInterface::class);
        $responseMock = $this->createMock(ResponseInterface::class);
        $token = $this->createMock(StravaTokenInterface::class);

        $stravaAPI = $container->get(StravaApi::class);
        self::assertInstanceOf(StravaApi::class, $stravaAPI);

        $stravaAPI->setClient($httpClientMock);

        $token
            ->expects(self::once())
            ->method('isAccessTokenValid')
            ->willReturn(true);

        $responseMock
            ->expects(self::once())
            ->method('getContent')
            ->willReturn(file_get_contents(dirname(__DIR__) . '/_Mock/athlete.json'));

        $httpClientMock
            ->expects(self::once())
            ->method('request')
            ->willReturn($responseMock);

        $athlete = $stravaAPI->getAthlete($token);

        self::assertInstanceOf(Athlete::class, $athlete);
    }
}
