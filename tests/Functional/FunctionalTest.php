<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Tests\Functional;

use NuBox\Strava\Api\Service\StravaApi;
use NuBox\Strava\Api\Tests\TestingKernel;
use PHPUnit\Framework\TestCase;

final class FunctionalTest extends TestCase
{
    public function testServiceWiring(): void
    {
        $kernel = new TestingKernel();
        $kernel->boot();
        $container = $kernel->getContainer();

        $stravaAPI = $container->get(StravaApi::class);

        self::assertInstanceOf(StravaApi::class, $stravaAPI);
    }
}
