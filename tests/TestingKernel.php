<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Tests;

use Exception;
use NuBox\Strava\Api\StravaApiBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\Generator\UrlGenerator;

final class TestingKernel extends Kernel
{
    use MicroKernelTrait;

    private array $stravaApiConfig;

    public function __construct(array $config = [])
    {
        $this->stravaApiConfig = $config;

        parent::__construct('test', true);
    }

    public function registerBundles(): iterable
    {
        return [
            new StravaApiBundle(),
            new FrameworkBundle(),
        ];
    }

    /**
     * @throws Exception
     */
    public function registerContainerConfiguration(LoaderInterface $loader): void
    {
        $loader->load(function (ContainerBuilder $containerBuilder) {
            $containerBuilder->register('router', UrlGenerator::class);

            $containerBuilder->loadFromExtension('strava_api', $this->stravaApiConfig);
        });
    }

    public function getCacheDir(): string
    {
        return __DIR__ . '/var/cache/' . spl_object_hash($this);
    }

    public function getLogDir(): string
    {
        return __DIR__ . '/var/logs/' . spl_object_hash($this);
    }
}
