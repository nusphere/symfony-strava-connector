<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Tests\DataObject;

use NuBox\Strava\Api\DataObject\Enum\GearType;
use NuBox\Strava\Api\DataObject\Enum\ResourceState;
use NuBox\Strava\Api\DataObject\Gear;
use PHPUnit\Framework\TestCase;

final class GearTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testGear(array $gearArray, GearType $gearType): void
    {
        $gear = new Gear($gearArray);

        self::assertSame($gearArray['id'], $gear->getId());
        self::assertSame($gearArray['primary'], $gear->isPrimary());
        self::assertSame($gearArray['name'], $gear->getName());
        self::assertSame($gearArray['nickname'], $gear->getNickName());
        self::assertSame(ResourceState::from($gearArray['resource_state']), $gear->getResourceState());
        self::assertSame($gearArray['retired'], $gear->isRetired());
        self::assertSame($gearArray['distance'], $gear->getDistance());
        self::assertSame($gearArray['converted_distance'], $gear->getConvertedDistance());

        self::assertSame($gearType, $gear->getGearType());
    }

    public function dataProvider(): array
    {
        return [
            "ResourceState::SUMMARY SHOE" => [
                [
                    "id" => "g9999999",
                    "primary" => true,
                    "name" => "Running shoe product",
                    "nickname" => "Running shoe",
                    "resource_state" => 2,
                    "retired" => false,
                    "distance" => 333333,
                    "converted_distance" => 333.333,
                ],
                GearType::SHOE
            ],

            "ResourceState::SUMMARY BIKE" => [
                [
                    "id" => "b9999999",
                    "primary" => true,
                    "name" => "Racing bike product",
                    "nickname" => "Running shoe",
                    "resource_state" => 2,
                    "retired" => true,
                    "distance" => 999999,
                    "converted_distance" => 999.999,
                ],
                GearType::BIKE
            ]
        ];
    }
}
