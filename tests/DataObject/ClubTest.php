<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Tests\DataObject;

use NuBox\Strava\Api\DataObject\Club;
use NuBox\Strava\Api\DataObject\Enum\GearType;
use NuBox\Strava\Api\DataObject\Enum\Membership;
use NuBox\Strava\Api\DataObject\Enum\ResourceState;
use NuBox\Strava\Api\DataObject\Enum\SportType;
use PHPUnit\Framework\TestCase;

final class ClubTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testGear(array $clubArray): void
    {
        $club = new Club($clubArray);

        self::assertSame($clubArray['id'], $club->getId());
        self::assertSame(ResourceState::from($clubArray['resource_state']), $club->getResourceState());
        self::assertSame($clubArray['name'], $club->getName());
        self::assertSame($clubArray['profile_medium'], $club->getProfileMedium());
        self::assertSame($clubArray['profile'], $club->getProfile());
        self::assertSame($clubArray['cover_photo'], $club->getCoverPhoto());
        self::assertSame($clubArray['cover_photo_small'], $club->getCoverPhotoSmall());
        self::assertSame(SportType::from($clubArray['sport_type']), $club->getSportType());
        self::assertSame($clubArray['city'], $club->getCity());
        self::assertSame($clubArray['state'], $club->getState());
        self::assertSame($clubArray['country'], $club->getCountry());
        self::assertSame($clubArray['private'], $club->isPrivate());
        self::assertSame($clubArray['member_count'], $club->getMemberCount());
        self::assertSame($clubArray['featured'], $club->isFeatured());
        self::assertSame($clubArray['verified'], $club->isVerified());
        self::assertSame($clubArray['url'], $club->getUrl());
        self::assertSame($clubArray['verified'], $club->isVerified());
        self::assertSame(Membership::from($clubArray['membership']), $club->getMembership());
        self::assertSame($clubArray['admin'], $club->isAdmin());
        self::assertSame($clubArray['owner'], $club->isOwner());

        foreach ($club->getDimensions() as $dimension) {
            self::assertContains($dimension->value, $clubArray['dimensions']);
        }

        foreach ($club->getActivityTypes() as $activityType) {
            self::assertContains($activityType->value, $clubArray['activity_types']);
        }
    }

    public function dataProvider(): array
    {
        return [
            "ResourceState::SUMMARY" => [
                [
                    "id" => 999999,
                    "resource_state" => 2,
                    "name" => "club name",
                    "profile_medium" => "club_url/medium.jpg",
                    "profile" => "club_url/large.jpg",
                    "cover_photo" => "club_url/large.jpg",
                    "cover_photo_small" => "club_url/small.jpg",
                    "activity_types" => [
                        "Handcycle",
                        "EBikeRide",
                        "VirtualRide",
                        "Velomobile",
                        "Ride"
                    ],
                    "activity_types_icon" => "sports_bike_normal",
                    "dimensions" => [
                        "distance",
                        "num_activities",
                        "best_activities_distance",
                        "velocity",
                        "elev_gain",
                    ],
                    "sport_type" => "cycling",
                    "city" => "City",
                    "state" => "State",
                    "country" => "Germany",
                    "private" => false,
                    "member_count" => 0,
                    "featured" => false,
                    "verified" => false,
                    "url" => "url",
                    "membership" => "member",
                    "admin" => false,
                    "owner" => false,
                ]
            ],
        ];
    }
}
