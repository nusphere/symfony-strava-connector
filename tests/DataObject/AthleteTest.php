<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Tests\DataObject;

use NuBox\Strava\Api\DataObject\Athlete;
use NuBox\Strava\Api\DataObject\Club;
use NuBox\Strava\Api\DataObject\Enum\AthleteType;
use NuBox\Strava\Api\DataObject\Enum\DateFormat;
use NuBox\Strava\Api\DataObject\Enum\Measurement;
use NuBox\Strava\Api\DataObject\Enum\ResourceState;
use NuBox\Strava\Api\DataObject\Enum\Sex;
use NuBox\Strava\Api\DataObject\Gear;
use PHPUnit\Framework\TestCase;

final class AthleteTest extends TestCase
{
    /**
     * @dataProvider dataProvider
     */
    public function testAthlete(array $athleteArray): void
    {
        $athlete = new Athlete($athleteArray);

        self::assertSame($athleteArray['id'], $athlete->getId());
        self::assertSame($athleteArray['username'], $athlete->getUserName());
        self::assertSame(ResourceState::from($athleteArray['resource_state']), $athlete->getResourceState());
        self::assertSame($athleteArray['firstname'], $athlete->getFirstName());
        self::assertSame($athleteArray['lastname'], $athlete->getLastName());
        self::assertSame($athleteArray['bio'], $athlete->getBio());
        self::assertSame($athleteArray['city'], $athlete->getCity());
        self::assertSame($athleteArray['state'], $athlete->getState());
        self::assertSame($athleteArray['country'], $athlete->getCountry());
        self::assertSame(Sex::from($athleteArray['sex']), $athlete->getSex());
        self::assertSame($athleteArray['summit'], $athlete->isSummit());
        self::assertEquals(new \DateTime($athleteArray['created_at']), $athlete->getCreatedAt());
        self::assertEquals(new \DateTime($athleteArray['updated_at']), $athlete->getUpdatedAt());
        self::assertSame($athleteArray['badge_type_id'], $athlete->getBadgeTypeId());
        self::assertSame($athleteArray['weight'], $athlete->getWeight());
        self::assertSame($athleteArray['profile_medium'], $athlete->getProfileMedium());
        self::assertSame($athleteArray['profile'], $athlete->getProfile());

        if ($athleteArray['resource_state'] === 3) {
            self::assertSame($athleteArray['blocked'], $athlete->isBlocked());
            self::assertSame($athleteArray['can_follow'], $athlete->canFollow());
            self::assertSame($athleteArray['follower_count'], $athlete->getFollowerCount());
            self::assertSame($athleteArray['friend_count'], $athlete->getFriendCount());
            self::assertSame($athleteArray['mutual_friend_count'], $athlete->getMutualFriendCount());
            self::assertSame(AthleteType::from($athleteArray['athlete_type']), $athlete->getAthleteType());
            self::assertSame(DateFormat::from($athleteArray['date_preference']), $athlete->getDatePreference());
            self::assertSame(Measurement::from($athleteArray['measurement_preference']), $athlete->getMeasurementPreference());

            self::assertCount(count($athleteArray['clubs']), $athlete->getClubs());
            foreach ($athlete->getClubs() as $club) {
                self::assertInstanceOf(Club::class, $club);
            }

            self::assertCount(count($athleteArray['bikes']), $athlete->getBikes());
            foreach ($athlete->getBikes() as $bike) {
                self::assertInstanceOf(Gear::class, $bike);
            }

            self::assertCount(count($athleteArray['shoes']), $athlete->getShoes());
            foreach ($athlete->getShoes() as $shoe) {
                self::assertInstanceOf(Gear::class, $shoe);
            }
        }
    }

    public function dataProvider(): array
    {
        return [
            "ResourceState::DETAIL" => [
                [
                    "id" => 99999999,
                    "username" => "testuser",
                    "resource_state" => 3,
                    "firstname" => "John",
                    "lastname" => "Doe",
                    "bio" => "some bio about the athlete",
                    "city" => "München",
                    "state" => "Bayer",
                    "country" => "Germany",
                    "sex" => "M",
                    "summit" => true,
                    "created_at" => "2017-08-10T20:54:48Z",
                    "updated_at" => "2022-03-04T07:09:28Z",
                    "badge_type_id" => 1,
                    "weight" => 114.6,
                    "profile_medium" => "profile_url/medium.jpg",
                    "profile" => "profile_url/large.jpg",
                    "blocked" => false,
                    "can_follow" => true,
                    "follower_count" => 44,
                    "friend_count" => 60,
                    "mutual_friend_count" => 0,
                    "athlete_type" => 1,
                    "date_preference" => "%m/%d/%Y",
                    "measurement_preference" => "meters",
                    "clubs" => [[
                        "id" => 123456,
                        "resource_state" => 2,
                        "name" => "CLUBNAME",
                        "profile_medium" => "club_url/medium.jpg",
                        "profile" => "club_url/large.jpg",
                        "cover_photo" => "club_url/large.jpg",
                        "cover_photo_small" => "club_url/small.jpg",
                        "activity_types" => [
                            "Handcycle",
                            "EBikeRide",
                            "VirtualRide",
                            "Velomobile",
                            "Ride"
                        ],
                        "activity_types_icon" => "sports_bike_normal",
                        "dimensions" => [
                            "distance",
                            "num_activities",
                            "best_activities_distance",
                            "velocity",
                            "elev_gain",
                        ],
                        "sport_type" => "cycling",
                        "city" => "Hannover",
                        "state" => "Niedersachsen",
                        "country" => "Germany",
                        "private" => false,
                        "member_count" => 0,
                        "featured" => false,
                        "verified" => false,
                        "url" => "club_URL",
                        "membership" => "member",
                        "admin" => false,
                        "owner" => false,
                    ]],
                    "bikes" => [[
                        "id" => "b9999999",
                        "primary" => true,
                        "name" => "Bike product name",
                        "nickname" => "Racing bike",
                        "resource_state" => 2,
                        "retired" => false,
                        "distance" => 999999,
                        "converted_distance" => 999.9999999,
                    ]],
                    "shoes" => [[
                        "id" => "g999999",
                        "primary" => true,
                        "name" => "Running shoe product name",
                        "nickname" => "Running shoe",
                        "resource_state" => 2,
                        "retired" => false,
                        "distance" => 999999,
                        "converted_distance" => 999.9999999,
                    ]],
                ]
            ],
            "ResourceState::SUMMARY" => [
                [
                    "id" => 99999999,
                    "username" => "testuser",
                    "resource_state" => 2,
                    "firstname" => "John",
                    "lastname" => "Doe",
                    "bio" => "some bio",
                    "city" => "Berlin",
                    "state" => "Berlin",
                    "country" => "Germany",
                    "sex" => "F",
                    "summit" => false,
                    "created_at" => "2017-08-10T20:54:48Z",
                    "updated_at" => "2022-03-04T07:09:28Z",
                    "badge_type_id" => 1,
                    "weight" => 62.0,
                    "profile_medium" => "profile_url/medium.jpg",
                    "profile" => "profile_url/large.jpg",
                    "friend" => null,
                    "follower" => null,
                ]
            ]
        ];
    }
}
