<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject;

use NuBox\Strava\Api\DataObject\Enum\ResourceState;

final class Map
{
    private string $id;
    private ?string $polyline;
    private ResourceState $resourceState;
    private ?string $summaryPolyline;

    public function __construct(array $mapArray)
    {
        $this->setId($mapArray['id']);
        $this->setResourceState(ResourceState::from($mapArray['resource_state']));
        $this->setPolyline($mapArray['polyline']);

        if (array_key_exists('summary_polyline', $mapArray)) {
            $this->setSummaryPolyline($mapArray['summary_polyline']);
        }
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): Map
    {
        $this->id = $id;

        return $this;
    }

    public function getPolyline(): ?string
    {
        return $this->polyline;
    }

    public function setPolyline(?string $polyline): Map
    {
        $this->polyline = $polyline;

        return $this;
    }

    public function getResourceState(): ResourceState
    {
        return $this->resourceState;
    }

    public function setResourceState(ResourceState $resourceState): Map
    {
        $this->resourceState = $resourceState;

        return $this;
    }

    public function getSummaryPolyline(): ?string
    {
        return $this->summaryPolyline;
    }

    public function setSummaryPolyline(?string $summaryPolyline): Map
    {
        $this->summaryPolyline = $summaryPolyline;

        return $this;
    }
}
