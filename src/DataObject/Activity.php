<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject;

use DateTime;
use DateTimeInterface;
use Exception;
use NuBox\Strava\Api\DataObject\Enum\ActivityType;
use NuBox\Strava\Api\DataObject\Enum\ResourceState;
use NuBox\Strava\Api\DataObject\Enum\StatType;
use NuBox\Strava\Api\DataObject\Enum\Visibility;
use NuBox\Strava\Api\DataObject\Enum\Zone;

final class Activity
{
    private int $id;
    private ResourceState $resourceState;
    private Athlete $athlete;
    private ?string $name;
    private ?float $distance;
    private ?int $movingTime;
    private ?int $elapsedTime;
    private ?float $totalElevationGain;
    private ?ActivityType $activityType;
    private ?DateTimeInterface $startDate;
    private ?DateTimeInterface $startDateLocal;
    private ?string $timeZone;
    private ?float $utcOffset;
    private ?string $locationCity;
    private ?string $locationState;
    private ?string $locationCountry;
    private ?int $achievementCount = 0;
    private ?int $kudosCount = 0;
    private ?int $commentCount = 0;
    private ?int $athleteCount = 0;
    private ?int $photoCount = 0;
    private ?Map $map;
    private ?bool $trainer;
    private ?bool $commute;
    private ?bool $manual;
    private ?bool $private;
    private ?Visibility $visibility;
    private ?bool $flagged;
    private ?string $gearId;
    private ?LatLng $startLatLng;
    private ?LatLng $endLatLng;
    private ?float $averageSpeed;
    private ?float $maxSpeed;
    private ?float $averageCadence;
    private ?bool $hasHeartRate;
    private ?float $averageHeartRate;
    private ?float $maxHeartRate;
    private ?bool $heartRateOptOut;
    private ?bool $displayHideHeartRateOption;
    private ?float $elevHigh;
    private ?float $elevLow;
    private ?int $uploadId;
    private ?string $uploadIdStr;
    private ?string $externalId;
    private ?bool $fromAcceptedTag;
    private ?int $prCount;
    private ?int $totalPhotoCount;
    private ?bool $hasKudoed;
    private ?float $sufferScore;
    private ?string $description;
    private ?float $calories;
    private ?string $perceivedExertion;
    private ?bool $preferPerceivedExertion;
    private ?Gear $gear;
    private ?bool $hideFromHome;
    private ?string $deviceName;
    private ?string $embedToken;
    private ?string $privateNote;

    private iterable $photos = []; //TODO: deep research
    
    /** @var Effort[] */
    private iterable $segmentEfforts = [];

    /** @var Segment[]  */
    private iterable $splitMetrics = [];

    /** @var Segment[]  */
    private iterable $splitStandards = [];

    /** @var Lap[]  */
    private iterable $laps = [];

    /** @var array{type: StatType, visibility: Visibility}[] */
    private iterable $statsVisibility = [];

    /** @var Zone[] */
    private iterable $availableZones = [];

    /**
     * @throws Exception
     */
    public function __construct(array $activityArray)
    {
        $this->setId($activityArray['id']);
        $this->setResourceState(ResourceState::from($activityArray['resource_state']));

        if ($this->getResourceState()->value >= ResourceState::SUMMARY->value) {
            $this->setAthlete(new Athlete($activityArray['athlete']));
            $this->setName($activityArray['name']);
            $this->setDistance($activityArray['distance']);
            $this->setMovingTime($activityArray['moving_time']);
            $this->setElapsedTime($activityArray['elapsed_time']);
            $this->setTotalElevationGain($activityArray['total_elevation_gain']);
            $this->setActivityType(ActivityType::from($activityArray['type']));
            $this->setStartDate(new DateTime($activityArray['start_date']));
            $this->setStartDateLocal(new DateTime($activityArray['start_date_local']));
            $this->setTimeZone($activityArray['timezone']);
            $this->setUtcOffset($activityArray['utc_offset']);
            $this->setLocationCity($activityArray['location_city']);
            $this->setLocationState($activityArray['location_state']);
            $this->setLocationCountry($activityArray['location_country']);
            $this->setAchievementCount($activityArray['achievement_count']);
            $this->setKudosCount($activityArray['kudos_count']);
            $this->setCommentCount($activityArray['comment_count']);
            $this->setAthleteCount($activityArray['athlete_count']);
            $this->setPhotoCount($activityArray['photo_count']);
            $this->setMap(new Map($activityArray['map']));
            $this->setTrainer($activityArray['trainer']);
            $this->setCommute($activityArray['commute']);
            $this->setManual($activityArray['manual']);
            $this->setPrivate($activityArray['private']);
            $this->setVisibility(Visibility::from($activityArray['visibility']));
            $this->setFlagged($activityArray['flagged']);

            if (is_string($activityArray['gear_id'])) {
                $this->setGearId($activityArray['gear_id']);
            }

            $this->setStartLatLng(new LatLng($activityArray['start_latlng']));
            $this->setEndLatLng(new LatLng($activityArray['end_latlng']));

            $this->setAverageSpeed($activityArray['average_speed']);
            $this->setMaxSpeed($activityArray['max_speed']);

            if (array_key_exists('average_cadence', $activityArray)) {
                $this->setAverageCadence($activityArray['average_cadence']);
            }
            $this->setHasHeartRate($activityArray['has_heartrate']);

            if (array_key_exists('average_heartrate', $activityArray)) {
                $this->setAverageHeartRate($activityArray['average_heartrate']);
            }

            if (array_key_exists('max_heartrate', $activityArray)) {
                $this->setMaxHeartRate($activityArray['max_heartrate']);
            }

            $this->setHeartRateOptOut($activityArray['heartrate_opt_out']);
            $this->setDisplayHideHeartRateOption($activityArray['display_hide_heartrate_option']);


            if (array_key_exists('elev_high', $activityArray)) {
                $this->setElevHigh($activityArray['elev_high']);
            }
            if (array_key_exists('elev_low', $activityArray)) {
                $this->setElevLow($activityArray['elev_low']);
            }
            $this->setUploadId($activityArray['upload_id']);

            if (array_key_exists('upload_id_str', $activityArray)) {
                $this->setUploadIdStr($activityArray['upload_id_str']);
            }
            $this->setExternalId($activityArray['external_id']);
            $this->setFromAcceptedTag($activityArray['from_accepted_tag']);
            $this->setPrCount($activityArray['pr_count']);
            $this->setTotalPhotoCount($activityArray['total_photo_count']);
            $this->setHasKudoed($activityArray['has_kudoed']);

            if (array_key_exists('suffer_score', $activityArray)) {
                $this->setSufferScore($activityArray['suffer_score']);
            }
            $this->setDescription($activityArray['description']);
            $this->setCalories($activityArray['calories']);
            $this->setPerceivedExertion($activityArray['perceived_exertion']);
            $this->setPreferPerceivedExertion($activityArray['prefer_perceived_exertion']);

            if (array_key_exists('gear', $activityArray)) {
                $this->setGear(new Gear($activityArray['gear']));
            }
            $this->setPhotos($activityArray['photos']);
            $this->setHideFromHome($activityArray['hide_from_home']);

            if (array_key_exists('device_name', $activityArray)) {
                $this->setDeviceName($activityArray['device_name']);
            }
            $this->setEmbedToken($activityArray['embed_token']);
            $this->setPrivateNote($activityArray['private_note']);

            foreach ($activityArray['segment_efforts'] as $segmentEffort) {
                $this->addSegmentEffort(new Effort($segmentEffort));
            }

            if (array_key_exists('splits_metric', $activityArray)) {
                foreach ($activityArray['splits_metric'] as $splitMetric) {
                    $this->addSplitMetric(new Segment($splitMetric));
                }
            }

            if (array_key_exists('splits_standard', $activityArray)) {
                foreach ($activityArray['splits_standard'] as $splitStandard) {
                    $this->addSplitStandard(new Segment($splitStandard));
                }
            }

            if (array_key_exists('laps', $activityArray)) {
                foreach ($activityArray['laps'] as $lap) {
                    $this->addLap(new Lap($lap));
                }
            }

            foreach ($activityArray['stats_visibility'] as $available_zone) {
                $this->addStatsVisibility([
                    'type' => StatType::from($available_zone['type']),
                    'visibility' => Visibility::from($available_zone['visibility']),
                ]);
            }

            foreach ($activityArray['available_zones'] as $available_zone) {
                $this->addAvailableZone(Zone::from($available_zone));
            }
        }
    }

    public function getResourceState(): ResourceState
    {
        return $this->resourceState;
    }

    public function setResourceState(ResourceState $resourceState): Activity
    {
        $this->resourceState = $resourceState;

        return $this;
    }

    public function getAthlete(): Athlete
    {
        return $this->athlete;
    }

    public function setAthlete(Athlete $athlete): Activity
    {
        $this->athlete = $athlete;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): Activity
    {
        $this->name = $name;

        return $this;
    }

    public function getDistance(): ?float
    {
        return $this->distance;
    }

    public function setDistance(float $distance): Activity
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * @return int
     */
    public function getMovingTime(): ?int
    {
        return $this->movingTime;
    }

    public function setMovingTime(int $movingTime): Activity
    {
        $this->movingTime = $movingTime;

        return $this;
    }

    public function getElapsedTime(): ?int
    {
        return $this->elapsedTime;
    }

    public function setElapsedTime(int $elapsedTime): Activity
    {
        $this->elapsedTime = $elapsedTime;

        return $this;
    }

    public function getTotalElevationGain(): ?float
    {
        return $this->totalElevationGain;
    }

    public function setTotalElevationGain(float $totalElevationGain): Activity
    {
        $this->totalElevationGain = $totalElevationGain;

        return $this;
    }

    public function getActivityType(): ?ActivityType
    {
        return $this->activityType;
    }

    public function setActivityType(ActivityType $activityType): Activity
    {
        $this->activityType = $activityType;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): Activity
    {
        $this->id = $id;

        return $this;
    }

    public function getStartDate(): ?DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(DateTimeInterface $startDate): Activity
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getStartDateLocal(): ?DateTimeInterface
    {
        return $this->startDateLocal;
    }

    public function setStartDateLocal(DateTimeInterface $startDateLocal): Activity
    {
        $this->startDateLocal = $startDateLocal;

        return $this;
    }

    public function getTimeZone(): ?string
    {
        return $this->timeZone;
    }

    public function setTimeZone(string $timeZone): Activity
    {
        $this->timeZone = $timeZone;

        return $this;
    }

    public function getUtcOffset(): ?float
    {
        return $this->utcOffset;
    }

    public function setUtcOffset(float $utcOffset): Activity
    {
        $this->utcOffset = $utcOffset;

        return $this;
    }

    public function getLocationCity(): ?string
    {
        return $this->locationCity;
    }

    public function setLocationCity(?string $locationCity): Activity
    {
        $this->locationCity = $locationCity;

        return $this;
    }

    public function getLocationState(): ?string
    {
        return $this->locationState;
    }

    public function setLocationState(?string $locationState): Activity
    {
        $this->locationState = $locationState;

        return $this;
    }

    public function getLocationCountry(): ?string
    {
        return $this->locationCountry;
    }

    public function setLocationCountry(string $locationCountry): Activity
    {
        $this->locationCountry = $locationCountry;

        return $this;
    }

    public function getAchievementCount(): ?int
    {
        return $this->achievementCount;
    }

    public function setAchievementCount(int $achievementCount): Activity
    {
        $this->achievementCount = $achievementCount;

        return $this;
    }

    public function getKudosCount(): ?int
    {
        return $this->kudosCount;
    }

    public function setKudosCount(int $kudosCount): Activity
    {
        $this->kudosCount = $kudosCount;

        return $this;
    }

    public function getCommentCount(): ?int
    {
        return $this->commentCount;
    }

    public function setCommentCount(int $commentCount): Activity
    {
        $this->commentCount = $commentCount;

        return $this;
    }

    public function getAthleteCount(): ?int
    {
        return $this->athleteCount;
    }

    public function setAthleteCount(int $athleteCount): Activity
    {
        $this->athleteCount = $athleteCount;

        return $this;
    }

    public function getPhotoCount(): ?int
    {
        return $this->photoCount;
    }

    public function setPhotoCount(int $photoCount): Activity
    {
        $this->photoCount = $photoCount;

        return $this;
    }

    public function getMap(): ?Map
    {
        return $this->map;
    }

    public function setMap(Map $map): Activity
    {
        $this->map = $map;

        return $this;
    }

    /**
     * @return Zone[]
     */
    public function getAvailableZones(): iterable
    {
        return $this->availableZones;
    }

    public function addAvailableZone(Zone $availableZone): self
    {
        $this->availableZones[] = $availableZone;

        return $this;
    }

    /**
     * @return array{type: StatType, visibility: Visibility}[]
     */
    public function getStatsVisibility(): iterable
    {
        return $this->statsVisibility;
    }

    /**
     * @param array{type: StatType, visibility: Visibility} $statsVisibility
     * @return Activity
     */
    public function addStatsVisibility(array $statsVisibility): self
    {
        $this->statsVisibility[] = $statsVisibility;

        return $this;
    }

    public function isTrainer(): ?bool
    {
        return $this->trainer;
    }

    public function setTrainer(bool $trainer): Activity
    {
        $this->trainer = $trainer;

        return $this;
    }

    public function isCommute(): ?bool
    {
        return $this->commute;
    }

    public function setCommute(bool $commute): Activity
    {
        $this->commute = $commute;

        return $this;
    }

    public function isPrivate(): ?bool
    {
        return $this->private;
    }

    public function setPrivate(bool $private): Activity
    {
        $this->private = $private;

        return $this;
    }

    public function getVisibility(): ?Visibility
    {
        return $this->visibility;
    }

    public function setVisibility(Visibility $visibility): Activity
    {
        $this->visibility = $visibility;

        return $this;
    }

    public function isFlagged(): ?bool
    {
        return $this->flagged;
    }

    public function setFlagged(bool $flagged): Activity
    {
        $this->flagged = $flagged;

        return $this;
    }

    public function getGearId(): ?string
    {
        return $this->gearId;
    }

    public function setGearId(string $gearId): Activity
    {
        $this->gearId = $gearId;

        return $this;
    }

    public function getStartLatLng(): ?LatLng
    {
        return $this->startLatLng;
    }

    public function setStartLatLng(LatLng $startLatLng): Activity
    {
        $this->startLatLng = $startLatLng;

        return $this;
    }

    public function getEndLatLng(): ?LatLng
    {
        return $this->endLatLng;
    }

    public function setEndLatLng(LatLng $endLatLng): Activity
    {
        $this->endLatLng = $endLatLng;

        return $this;
    }

    public function isManual(): ?bool
    {
        return $this->manual;
    }

    public function setManual(bool $manual): void
    {
        $this->manual = $manual;
    }

    public function getAverageSpeed(): ?float
    {
        return $this->averageSpeed;
    }

    public function setAverageSpeed(float $averageSpeed): Activity
    {
        $this->averageSpeed = $averageSpeed;

        return $this;
    }

    public function getMaxSpeed(): ?float
    {
        return $this->maxSpeed;
    }

    public function setMaxSpeed(float $maxSpeed): Activity
    {
        $this->maxSpeed = $maxSpeed;

        return $this;
    }

    public function getAverageCadence(): ?float
    {
        return $this->averageCadence;
    }

    public function setAverageCadence(float $averageCadence): Activity
    {
        $this->averageCadence = $averageCadence;

        return $this;
    }

    public function isHasHeartRate(): ?bool
    {
        return $this->hasHeartRate;
    }

    public function setHasHeartRate(bool $hasHeartRate): Activity
    {
        $this->hasHeartRate = $hasHeartRate;

        return $this;
    }

    public function getAverageHeartRate(): ?float
    {
        return $this->averageHeartRate;
    }

    public function setAverageHeartRate(float $averageHeartRate): Activity
    {
        $this->averageHeartRate = $averageHeartRate;

        return $this;
    }

    public function getMaxHeartRate(): ?float
    {
        return $this->maxHeartRate;
    }

    public function setMaxHeartRate(float $maxHeartRate): Activity
    {
        $this->maxHeartRate = $maxHeartRate;

        return $this;
    }

    public function isHeartRateOptOut(): ?bool
    {
        return $this->heartRateOptOut;
    }

    public function setHeartRateOptOut(bool $heartRateOptOut): Activity
    {
        $this->heartRateOptOut = $heartRateOptOut;

        return $this;
    }

    public function isDisplayHideHeartRateOption(): ?bool
    {
        return $this->displayHideHeartRateOption;
    }

    public function setDisplayHideHeartRateOption(bool $displayHideHeartRateOption): Activity
    {
        $this->displayHideHeartRateOption = $displayHideHeartRateOption;

        return $this;
    }

    public function getElevHigh(): ?float
    {
        return $this->elevHigh;
    }

    public function setElevHigh(float $elevHigh): Activity
    {
        $this->elevHigh = $elevHigh;

        return $this;
    }

    public function getElevLow(): ?float
    {
        return $this->elevLow;
    }

    public function setElevLow(float $elevLow): Activity
    {
        $this->elevLow = $elevLow;

        return $this;
    }

    public function getUploadId(): ?int
    {
        return $this->uploadId;
    }

    public function setUploadId(?int $uploadId): Activity
    {
        $this->uploadId = $uploadId;

        return $this;
    }

    public function getUploadIdStr(): ?string
    {
        return $this->uploadIdStr;
    }

    public function setUploadIdStr(?string $uploadIdStr): Activity
    {
        $this->uploadIdStr = $uploadIdStr;

        return $this;
    }

    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    public function setExternalId(?string $externalId): Activity
    {
        $this->externalId = $externalId;

        return $this;
    }

    public function isFromAcceptedTag(): ?bool
    {
        return $this->fromAcceptedTag;
    }

    public function setFromAcceptedTag(bool $fromAcceptedTag): Activity
    {
        $this->fromAcceptedTag = $fromAcceptedTag;

        return $this;
    }

    public function getPrCount(): ?int
    {
        return $this->prCount;
    }

    public function setPrCount(int $prCount): Activity
    {
        $this->prCount = $prCount;

        return $this;
    }

    public function getTotalPhotoCount(): ?int
    {
        return $this->totalPhotoCount;
    }

    public function setTotalPhotoCount(int $totalPhotoCount): Activity
    {
        $this->totalPhotoCount = $totalPhotoCount;

        return $this;
    }

    public function isHasKudoed(): ?bool
    {
        return $this->hasKudoed;
    }

    public function setHasKudoed(bool $hasKudoed): Activity
    {
        $this->hasKudoed = $hasKudoed;

        return $this;
    }

    public function getSufferScore(): ?float
    {
        return $this->sufferScore;
    }

    public function setSufferScore(float $sufferScore): Activity
    {
        $this->sufferScore = $sufferScore;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): Activity
    {
        $this->description = $description;

        return $this;
    }

    public function getCalories(): ?float
    {
        return $this->calories;
    }

    public function setCalories(float $calories): Activity
    {
        $this->calories = $calories;

        return $this;
    }

    public function getPerceivedExertion(): ?string
    {
        return $this->perceivedExertion;
    }

    public function setPerceivedExertion(?string $perceivedExertion): Activity
    {
        $this->perceivedExertion = $perceivedExertion;

        return $this;
    }

    /**
     * @return Effort[]
     */
    public function getSegmentEfforts(): iterable
    {
        return $this->segmentEfforts;
    }

    public function addSegmentEffort(Effort $segmentEffort): Activity
    {
        $this->segmentEfforts[] = $segmentEffort;

        return $this;
    }

    public function isPreferPerceivedExertion(): ?bool
    {
        return $this->preferPerceivedExertion;
    }

    public function setPreferPerceivedExertion(?bool $preferPerceivedExertion): self
    {
        $this->preferPerceivedExertion = $preferPerceivedExertion;

        return $this;
    }

    /**
     * @return Segment[]
     */
    public function getSplitMetrics(): iterable
    {
        return $this->splitMetrics;
    }

    public function addSplitMetric(Segment $splitMetric): self
    {
        $this->splitMetrics[] = $splitMetric;

        return $this;
    }

    /**
     * @return Segment[]
     */
    public function getSplitStandards(): iterable
    {
        return $this->splitStandards;
    }

    public function addSplitStandard(Segment $splitStandard): self
    {
        $this->splitStandards[] = $splitStandard;

        return $this;
    }

    /**
     * @return Lap[]
     */
    public function getLaps(): iterable
    {
        return $this->laps;
    }

    public function addLap(Lap $lap): self
    {
        $this->laps[] = $lap;

        return $this;
    }

    public function getGear(): Gear
    {
        return $this->gear;
    }

    public function setGear(Gear $gear): Activity
    {
        $this->gear = $gear;

        return $this;
    }

    /**
     * @return array
     */
    public function getPhotos(): array
    {
        return $this->photos;
    }

    /**
     * @param array $photos
     * @return Activity
     */
    public function setPhotos(array $photos): Activity
    {
        $this->photos = $photos;

        return $this;
    }

    public function isHideFromHome(): ?bool
    {
        return $this->hideFromHome;
    }

    public function setHideFromHome(bool $hideFromHome): Activity
    {
        $this->hideFromHome = $hideFromHome;

        return $this;
    }

    public function getDeviceName(): ?string
    {
        return $this->deviceName;
    }

    public function setDeviceName(string $deviceName): Activity
    {
        $this->deviceName = $deviceName;

        return $this;
    }

    public function getEmbedToken(): ?string
    {
        return $this->embedToken;
    }

    public function setEmbedToken(string $embedToken): Activity
    {
        $this->embedToken = $embedToken;

        return $this;
    }

    public function getPrivateNote(): ?string
    {
        return $this->privateNote;
    }

    public function setPrivateNote(?string $privateNote): Activity
    {
        $this->privateNote = $privateNote;

        return $this;
    }

}
