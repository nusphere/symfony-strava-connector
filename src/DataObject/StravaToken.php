<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject;

use DateTimeInterface;

final class StravaToken implements StravaTokenInterface
{
    private string $tokenType;
    private string $refresh_token;
    private string $access_token;

    private DateTimeInterface $expiresAt;
    private DateTimeInterface $expiresIn;

    private ?Athlete $athlete;
    private string $scopes;

    public function getTokenType(): string
    {
        return $this->tokenType;
    }

    public function setTokenType(string $tokenType): self
    {
        $this->tokenType = $tokenType;

        return $this;
    }

    public function getExpiresAt(): DateTimeInterface
    {
        return $this->expiresAt;
    }

    public function setExpiresAt(DateTimeInterface $expiresAt): self
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    public function getExpiresIn(): DateTimeInterface
    {
        return $this->expiresIn;
    }

    public function setExpiresIn(DateTimeInterface $expiresIn): self
    {
        $this->expiresIn = $expiresIn;

        return $this;
    }

    public function getRefreshToken(): string
    {
        return $this->refresh_token;
    }

    public function setRefreshToken(string $refreshToken): self
    {
        $this->refresh_token = $refreshToken;

        return $this;
    }

    public function getAccessToken(): string
    {
        return $this->access_token;
    }

    public function setAccessToken(string $accessToken): self
    {
        $this->access_token = $accessToken;

        return $this;
    }

    public function getAthlete(): ?Athlete
    {
        return $this->athlete;
    }

    public function setAthlete(Athlete $athlete): self
    {
        $this->athlete = $athlete;

        return $this;
    }

    public function getScopes(): string
    {
        return $this->scopes;
    }

    public function setScopes(string $scopes): self
    {
        $this->scopes = $scopes;

        return $this;
    }

    public function isAccessTokenValid(): bool
    {
        return $this->getExpiresAt() > new \DateTime();
    }
}
