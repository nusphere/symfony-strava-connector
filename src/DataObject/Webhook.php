<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject;

use DateTime;
use DateTimeInterface;
use Exception;
use NuBox\Strava\Api\DataObject\Enum\AspectType;
use NuBox\Strava\Api\DataObject\Enum\ObjectType;

final class Webhook
{
    private int $ownerId;
    private int $objectId;
    private int $subscriptionId;

    private ObjectType $objectType;
    private AspectType $aspectType;
    private DateTimeInterface $eventTime;

    /**
     * @var array<string, string>
     */
    private array $updates = [];

    /**
     * @param array{
     *     aspect_type: string,
     *     event_time: int,
     *     object_id: int,
     *     object_type: string,
     *     owner_id: int,
     *     subscription_id: int,
     *     updates: array<string, string>
     * } $webhook
     */
    public function __construct(array $webhook)
    {
        $this->setAspectType(AspectType::from($webhook['aspect_type']));
        $this->setObjectType(ObjectType::from($webhook['object_type']));
        $this->setObjectId($webhook['object_id']);
        $this->setOwnerId($webhook['owner_id']);
        $this->setSubscriptionId($webhook['subscription_id']);
        $this->setUpdates($webhook['updates']);

        try {
            $this->setEventTime(new DateTime('@' . $webhook['event_time']));
        } catch (Exception) {
            $this->setEventTime(new DateTime());
        }
    }

    public function getObjectType(): ObjectType
    {
        return $this->objectType;
    }

    public function setObjectType(ObjectType $objectType): void
    {
        $this->objectType = $objectType;
    }

    public function getObjectId(): int
    {
        return $this->objectId;
    }

    public function setObjectId(int $objectId): void
    {
        $this->objectId = $objectId;
    }

    public function getAspectType(): AspectType
    {
        return $this->aspectType;
    }

    public function setAspectType(AspectType $aspectType): void
    {
        $this->aspectType = $aspectType;
    }

    /**
     * @return array<string, string>
     */
    public function getUpdates(): array
    {
        return $this->updates;
    }

    /**
     * @param array<string, string> $updates
     */
    public function setUpdates(array $updates = []): void
    {
        $this->updates = $updates;
    }

    public function getOwnerId(): int
    {
        return $this->ownerId;
    }

    public function setOwnerId(int $ownerId): void
    {
        $this->ownerId = $ownerId;
    }

    public function getSubscriptionId(): int
    {
        return $this->subscriptionId;
    }

    public function setSubscriptionId(int $subscriptionId): void
    {
        $this->subscriptionId = $subscriptionId;
    }

    public function getEventTime(): DateTimeInterface
    {
        return $this->eventTime;
    }

    public function setEventTime(DateTimeInterface $eventTime): void
    {
        $this->eventTime = $eventTime;
    }
}
