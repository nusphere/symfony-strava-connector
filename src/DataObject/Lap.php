<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject;

use DateTime;
use DateTimeInterface;
use Exception;
use NuBox\Strava\Api\DataObject\Enum\ResourceState;

final class Lap
{
    private int $id;
    private ResourceState $resourceState;
    private ?string $name;
    private ?Activity $activity;
    private ?Athlete $athlete;
    private ?int $elapsedTime;
    private ?int $movingTime;
    private ?DateTimeInterface $startDate;
    private ?DateTimeInterface $startDateLocal;
    private ?float $distance;
    private ?int $startIndex;
    private ?int $endIndex;
    private ?float $totalElevationGain;
    private ?float $averageSpeed;
    private ?float $maxSpeed;
    private ?float $averageCadence;
    private ?bool $deviceWatts;
    private ?float $averageHeartRate;
    private ?float $maxHeartRate;
    private ?int $lapIndex;
    private ?int $split;

    /**
     * @throws Exception
     */
    public function __construct(array $lapArray)
    {
        $this->setId($lapArray['id']);
        $this->setResourceState(ResourceState::from($lapArray['resource_state']));

        if ($this->getResourceState()->value >= ResourceState::SUMMARY->value) {
            $this->setName($lapArray['name']);
            $this->setActivity(new Activity($lapArray['activity']));
            $this->setAthlete(new Athlete($lapArray['athlete']));
            $this->setElapsedTime($lapArray['elapsed_time']);
            $this->setMovingTime($lapArray['moving_time']);
            $this->setStartDate(new DateTime($lapArray['start_date']));
            $this->setStartDateLocal(new DateTime($lapArray['start_date_local']));
            $this->setDistance($lapArray['distance']);
            $this->setStartIndex($lapArray['start_index']);
            $this->setEndIndex($lapArray['end_index']);
            $this->setTotalElevationGain($lapArray['total_elevation_gain']);
            $this->setAverageSpeed($lapArray['average_speed']);
            $this->setMaxSpeed($lapArray['max_speed']);

            if (array_key_exists('average_cadence', $lapArray)) {
                $this->setAverageCadence($lapArray['average_cadence']);
            }

            $this->setDeviceWatts($lapArray['device_watts']);
            $this->setAverageHeartRate($lapArray['average_heartrate']);
            $this->setMaxHeartRate($lapArray['max_heartrate']);
            $this->setLapIndex($lapArray['lap_index']);
            $this->setSplit($lapArray['split']);
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Lap
    {
        $this->id = $id;

        return $this;
    }

    public function getResourceState(): ResourceState
    {
        return $this->resourceState;
    }

    public function setResourceState(ResourceState $resourceState): Lap
    {
        $this->resourceState = $resourceState;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): Lap
    {
        $this->name = $name;

        return $this;
    }

    public function getActivity(): ?Activity
    {
        return $this->activity;
    }

    public function setActivity(Activity $activity): Lap
    {
        $this->activity = $activity;

        return $this;
    }

    public function getAthlete(): ?Athlete
    {
        return $this->athlete;
    }

    public function setAthlete(Athlete $athlete): Lap
    {
        $this->athlete = $athlete;

        return $this;
    }

    public function getElapsedTime(): ?int
    {
        return $this->elapsedTime;
    }

    public function setElapsedTime(int $elapsedTime): Lap
    {
        $this->elapsedTime = $elapsedTime;

        return $this;
    }

    public function getMovingTime(): ?int
    {
        return $this->movingTime;
    }

    public function setMovingTime(int $movingTime): Lap
    {
        $this->movingTime = $movingTime;

        return $this;
    }

    public function getStartDate(): ?DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(DateTimeInterface $startDate): Lap
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getStartDateLocal(): ?DateTimeInterface
    {
        return $this->startDateLocal;
    }

    public function setStartDateLocal(DateTimeInterface $startDateLocal): Lap
    {
        $this->startDateLocal = $startDateLocal;

        return $this;
    }

    public function getDistance(): ?float
    {
        return $this->distance;
    }

    public function setDistance(float $distance): Lap
    {
        $this->distance = $distance;

        return $this;
    }

    public function getStartIndex(): ?int
    {
        return $this->startIndex;
    }

    public function setStartIndex(int $startIndex): Lap
    {
        $this->startIndex = $startIndex;

        return $this;
    }

    public function getEndIndex(): ?int
    {
        return $this->endIndex;
    }

    public function setEndIndex(int $endIndex): Lap
    {
        $this->endIndex = $endIndex;

        return $this;
    }

    public function getTotalElevationGain(): ?float
    {
        return $this->totalElevationGain;
    }

    public function setTotalElevationGain(float $totalElevationGain): Lap
    {
        $this->totalElevationGain = $totalElevationGain;

        return $this;
    }

    public function getAverageSpeed(): ?float
    {
        return $this->averageSpeed;
    }

    public function setAverageSpeed(float $averageSpeed): Lap
    {
        $this->averageSpeed = $averageSpeed;

        return $this;
    }

    public function getMaxSpeed(): ?float
    {
        return $this->maxSpeed;
    }

    public function setMaxSpeed(float $maxSpeed): Lap
    {
        $this->maxSpeed = $maxSpeed;

        return $this;
    }

    public function getAverageCadence(): ?float
    {
        return $this->averageCadence;
    }

    public function setAverageCadence(float $averageCadence): Lap
    {
        $this->averageCadence = $averageCadence;

        return $this;
    }

    public function isDeviceWatts(): ?bool
    {
        return $this->deviceWatts;
    }

    public function setDeviceWatts(bool $deviceWatts): Lap
    {
        $this->deviceWatts = $deviceWatts;

        return $this;
    }

    public function getAverageHeartRate(): ?float
    {
        return $this->averageHeartRate;
    }

    public function setAverageHeartRate(float $averageHeartRate): Lap
    {
        $this->averageHeartRate = $averageHeartRate;

        return $this;
    }

    public function getMaxHeartRate(): ?float
    {
        return $this->maxHeartRate;
    }

    public function setMaxHeartRate(float $maxHeartRate): Lap
    {
        $this->maxHeartRate = $maxHeartRate;

        return $this;
    }

    public function getLapIndex(): ?int
    {
        return $this->lapIndex;
    }

    public function setLapIndex(int $lapIndex): Lap
    {
        $this->lapIndex = $lapIndex;

        return $this;
    }

    public function getSplit(): ?int
    {
        return $this->split;
    }

    public function setSplit(int $split): Lap
    {
        $this->split = $split;

        return $this;
    }
}
