<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject;

use DateTimeInterface;
use NuBox\Strava\Api\DataObject\Enum\AthleteType;
use NuBox\Strava\Api\DataObject\Enum\DateFormat;
use NuBox\Strava\Api\DataObject\Enum\Measurement;
use NuBox\Strava\Api\DataObject\Enum\ResourceState;
use NuBox\Strava\Api\DataObject\Enum\Sex;

final class Athlete
{
    private int $id;
    private int $ftp;
    private int $badgeTypeId;
    private int $followerCount = 0;
    private int $friendCount = 0;
    private int $mutualFriendCount = 0;

    private string $userName;
    private string $firstName;
    private string $lastName;
    private string $bio;
    private string $city;
    private string $state;
    private string $country;
    private string $profileMedium;
    private string $profile;

    private bool $blocked;
    private bool $canFollow;
    private bool $summit;

    private float $weight;

    private AthleteType $athleteType;
    private Sex $sex;
    private ResourceState $resourceState;
    private Measurement $measurementPreference;
    private DateFormat $datePreference;
    private DateTimeInterface $createdAt;
    private DateTimeInterface $updatedAt;

    /** @var Club[] */
    private array $clubs = [];

    /** @var Gear[] */
    private array $bikes = [];

    /** @var Gear[] */
    private array $shoes = [];

    public function __construct(array $athlete)
    {
        $this->setId($athlete['id']);
        $this->setResourceState(ResourceState::from($athlete['resource_state']));

        if ($this->getResourceState()->value >= ResourceState::SUMMARY->value) {
            $this->setUserName($athlete['username']);
            $this->setFirstName($athlete['firstname']);
            $this->setLastName($athlete['lastname']);
            $this->setBio($athlete['bio']);
            $this->setCity($athlete['city']);
            $this->setState($athlete['state']);
            $this->setCountry($athlete['country']);
            $this->setSex(Sex::from($athlete['sex']));
            $this->setSummit($athlete['summit']);
            $this->setCreatedAt(new \DateTime($athlete['created_at']));
            $this->setUpdatedAt(new \DateTime($athlete['updated_at']));
            $this->setBadgeTypeId($athlete['badge_type_id']);
            $this->setWeight($athlete['weight']);
            $this->setProfileMedium($athlete['profile_medium']);
            $this->setProfile($athlete['profile']);

            if ($this->getResourceState()->value >= ResourceState::DETAIL->value) {
                $this->setBlocked($athlete['blocked']);
                $this->setCanFollow($athlete['can_follow']);
                $this->setFollowerCount($athlete['follower_count']);
                $this->setFriendCount($athlete['friend_count']);
                $this->setMutualFriendCount($athlete['mutual_friend_count']);
                $this->setAthleteType(AthleteType::from($athlete['athlete_type']));
                $this->setDatePreference(DateFormat::from($athlete['date_preference']));
                $this->setMeasurementPreference(Measurement::from($athlete['measurement_preference']));

                foreach ($athlete['clubs'] as $club) {
                    $this->addClub(new Club($club));
                }

                foreach ($athlete['bikes'] as $bike) {
                    $this->addBike(new Gear($bike));
                }

                foreach ($athlete['shoes'] as $shoe) {
                    $this->addShoe(new Gear($shoe));
                }
            }
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): Athlete
    {
        $this->id = $id;

        return $this;
    }

    public function getUserName(): string
    {
        return $this->userName;
    }

    public function setUserName(string $userName): Athlete
    {
        $this->userName = $userName;

        return $this;
    }

    public function getResourceState(): ResourceState
    {
        return $this->resourceState;
    }

    public function setResourceState(ResourceState $resourceState): Athlete
    {
        $this->resourceState = $resourceState;

        return $this;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): Athlete
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): Athlete
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city): Athlete
    {
        $this->city = $city;

        return $this;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function setState(string $state): Athlete
    {
        $this->state = $state;

        return $this;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): Athlete
    {
        $this->country = $country;

        return $this;
    }

    public function getSex(): Sex
    {
        return $this->sex;
    }

    public function setSex(Sex $sex): Athlete
    {
        $this->sex = $sex;

        return $this;
    }

    public function getWeight(): float
    {
        return $this->weight;
    }

    public function setWeight(float $weight): Athlete
    {
        $this->weight = $weight;

        return $this;
    }

    public function getFtp(): int
    {
        return $this->ftp;
    }

    public function setFtp(int $ftp): Athlete
    {
        $this->ftp = $ftp;

        return $this;
    }

    public function isSummit(): bool
    {
        return $this->summit;
    }

    public function setSummit(bool $summit): Athlete
    {
        $this->summit = $summit;

        return $this;
    }

    public function getMeasurementPreference(): Measurement
    {
        return $this->measurementPreference;
    }

    public function setMeasurementPreference(Measurement $measurementPreference): Athlete
    {
        $this->measurementPreference = $measurementPreference;

        return $this;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): Athlete
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeInterface $updatedAt): Athlete
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getFollowerCount(): int
    {
        return $this->followerCount;
    }

    public function setFollowerCount(int $followerCount): Athlete
    {
        $this->followerCount = $followerCount;

        return $this;
    }

    public function getFriendCount(): int
    {
        return $this->friendCount;
    }

    public function setFriendCount(int $friendCount): Athlete
    {
        $this->friendCount = $friendCount;

        return $this;
    }

    public function getBio(): string
    {
        return $this->bio;
    }

    public function setBio(string $bio): void
    {
        $this->bio = $bio;
    }

    public function getBadgeTypeId(): int
    {
        return $this->badgeTypeId;
    }

    public function setBadgeTypeId(int $badgeTypeId): void
    {
        $this->badgeTypeId = $badgeTypeId;
    }

    public function getProfileMedium(): string
    {
        return $this->profileMedium;
    }

    public function setProfileMedium(string $profileMedium): void
    {
        $this->profileMedium = $profileMedium;
    }

    public function getProfile(): string
    {
        return $this->profile;
    }

    public function setProfile(string $profile): void
    {
        $this->profile = $profile;
    }

    /**
     * @return DateFormat
     */
    public function getDatePreference(): DateFormat
    {
        return $this->datePreference;
    }

    /**
     * @param DateFormat $datePreference
     * @return Athlete
     */
    public function setDatePreference(DateFormat $datePreference): Athlete
    {
        $this->datePreference = $datePreference;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBlocked(): bool
    {
        return $this->blocked;
    }

    /**
     * @param bool $blocked
     * @return Athlete
     */
    public function setBlocked(bool $blocked): Athlete
    {
        $this->blocked = $blocked;

        return $this;
    }

    /**
     * @return bool
     */
    public function canFollow(): bool
    {
        return $this->canFollow;
    }

    /**
     * @param bool $canFollow
     * @return Athlete
     */
    public function setCanFollow(bool $canFollow): Athlete
    {
        $this->canFollow = $canFollow;

        return $this;
    }

    /**
     * @return int
     */
    public function getMutualFriendCount(): int
    {
        return $this->mutualFriendCount;
    }

    /**
     * @param int $mutualFriendCount
     * @return Athlete
     */
    public function setMutualFriendCount(int $mutualFriendCount): Athlete
    {
        $this->mutualFriendCount = $mutualFriendCount;

        return $this;
    }

    /**
     * @return AthleteType
     */
    public function getAthleteType(): AthleteType
    {
        return $this->athleteType;
    }

    /**
     * @param AthleteType $athleteType
     * @return Athlete
     */
    public function setAthleteType(AthleteType $athleteType): Athlete
    {
        $this->athleteType = $athleteType;

        return $this;
    }

    /**
     * @return Club[]
     */
    public function getClubs(): iterable
    {
        foreach ($this->clubs as $club) {
            yield $club;
        }
    }

    public function addClub(Club $club): Athlete
    {
        $this->clubs[] = $club;

        return $this;
    }

    /**
     * @return Gear[]
     */
    public function getBikes(): iterable
    {
        foreach ($this->bikes as $bike) {
            yield $bike;
        }
    }

    public function addBike(Gear $bike): Athlete
    {
        $this->bikes[] = $bike;

        return $this;
    }

    /**
     * @return Gear[]
     */
    public function getShoes(): iterable
    {
        foreach ($this->shoes as $shoe) {
            yield $shoe;
        }
    }

    public function addShoe(Gear $shoe): Athlete
    {
        $this->shoes[] = $shoe;

        return $this;
    }
}
