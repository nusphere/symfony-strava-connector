<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum AthleteType: int
{
    case STANDARD = 1;
}
