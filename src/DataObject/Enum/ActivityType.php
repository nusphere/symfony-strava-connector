<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum ActivityType: string
{
    case ALPINE_SKI = 'AlpineSki';
    case BACK_COUNTRY_SKI = 'BackcountrySki';
    case CANOEING = 'Canoeing';
    case CROSSFIT = 'Crossfit';
    case E_BIKE_RIDE = 'EBikeRide';
    case ELLIPTICAL = 'Elliptical';
    case GOLF = 'Golf';
    case HAND_CYCLE = 'Handcycle';
    case HIKE = 'Hike';
    case ICE_SKATE = 'IceSkate';
    case INLINE_SKATE = 'InlineSkate';
    case KAYAKING = 'Kayaking';
    case KITE_SURF = 'Kitesurf';
    case NORDIC_SKI = 'NordicSki';
    case RIDE = 'Ride';
    case ROCK_CLIMBING = 'RockClimbing';
    case ROLLER_SKI = 'RollerSki';
    case ROWING = 'Rowing';
    case RUN = 'Run';
    case SAIL = 'Sail';
    case SKATEBOARD = 'Skateboard';
    case SNOWBOARD = 'Snowboard';
    case SNOWSHOE = 'Snowshoe';
    case SOCCER = 'Soccer';
    case STAIR_STEPPER = 'StairStepper';
    case STAND_UP_PADDLING = 'StandUpPaddling';
    case SURFING = 'Surfing';
    case SWIM = 'Swim';
    case VELO_MOBILE = 'Velomobile';
    case VIRTUAL_RIDE = 'VirtualRide';
    case VIRTUAL_RUN = 'VirtualRun';
    case WALK = 'Walk';
    case WEIGHT_TRAINING = 'WeightTraining';
    case WHEEL_CHAIR = 'Wheelchair';
    case WINDSURF = 'Windsurf';
    case WORKOUT = 'Workout';
    case YOGA = 'Yoga';
}
