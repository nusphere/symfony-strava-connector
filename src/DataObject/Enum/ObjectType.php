<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum ObjectType: string
{
    case ACTIVITY = 'activity';
    case ATHLETE = 'athlete';
}
