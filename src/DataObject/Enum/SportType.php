<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum SportType: string
{
    case CYCLING = 'cycling';
    case RUNNING = 'running';
    case TRIATHLON = 'triathlon';
    case OTHER = 'other';
}
