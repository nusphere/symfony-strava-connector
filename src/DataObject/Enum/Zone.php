<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum Zone: string
{
    case HEART_RATE = 'heartrate';
    case PACE = 'pace';
}
