<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum Dimension: string
{
    case DISTANCE = "distance";
    case NUM_ACTIVITIES = "num_activities";
    case BEST_ACTIVITIES_DISTANCE = "best_activities_distance";
    case BEST_ACTIVITIES_MOVING_TIME = "best_activities_moving_time";
    case VELOCITY = "velocity";
    case ELEV_GAIN = "elev_gain";
    case MOVING_TIME = "moving_time";
    case SWIM_TIME = "swim_time";
    case RIDE_TIME = "ride_time";
    case RUN_TIME = "run_time";
}
