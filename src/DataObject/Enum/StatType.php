<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum StatType: string
{
    case HEART_RATE = "heart_rate";
    case PACE = "pace";
    case POWER = "power";
    case SPEED = "speed";
    case CALORIES = "calories";
}
