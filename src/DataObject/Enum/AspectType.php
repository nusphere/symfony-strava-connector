<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum AspectType: string
{
    case UPDATE = 'update';
    case CREATE = 'create';
    case DELETE = 'delete';
}
