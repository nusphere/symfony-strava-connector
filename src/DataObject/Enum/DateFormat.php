<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum DateFormat: string
{
    case MMDDYYYY = '%m/%d/%Y';
    case DDMMYYYY = '%d/%m/%Y';
    case YYYYMMDD = '%Y/%m/%d';
    case YYYYDDMM = '%Y/%d/%m';
}
