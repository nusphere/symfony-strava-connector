<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum Measurement: string
{
    case FEET = 'feet';
    case METERS = 'meters';
}
