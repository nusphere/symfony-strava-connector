<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum Sex: string
{
    case MALE = 'M';
    case FEMALE = 'F';
}
