<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum Membership: string
{
    case MEMBER = 'member';
    case PENDING = 'pending';
}
