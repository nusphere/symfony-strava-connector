<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum ActivityTypeIcon: string
{
    case BIKE = "sports_bike_normal";
    case TRIATHLON = "sports_triathlon_normal";
}
