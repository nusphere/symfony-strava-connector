<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum ResourceState: int
{
    case META = 1;
    case SUMMARY = 2;
    case DETAIL = 3;
}
