<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum Visibility: string
{
    case PRIVATE = 'only_me';
    case EVERYONE = 'everyone';
    case FOLLOWERS = 'followers_only';
}
