<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject\Enum;

enum GearType: string
{
    case BIKE = 'b';
    case SHOE = 'g';
}
