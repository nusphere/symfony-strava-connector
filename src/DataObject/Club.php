<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject;

use NuBox\Strava\Api\DataObject\Enum\ActivityType;
use NuBox\Strava\Api\DataObject\Enum\ActivityTypeIcon;
use NuBox\Strava\Api\DataObject\Enum\Dimension;
use NuBox\Strava\Api\DataObject\Enum\Membership;
use NuBox\Strava\Api\DataObject\Enum\ResourceState;
use NuBox\Strava\Api\DataObject\Enum\SportType;

final class Club
{
    private int $id;
    private ResourceState $resourceState;
    private string $name;
    private string $profile;
    private string $profileMedium;
    private ?string $coverPhoto;
    private ?string $coverPhotoSmall;
    private SportType $sportType;
    private string $city;
    private string $state;
    private string $country;
    private bool $private;
    private int $memberCount;
    private bool $featured;
    private bool $verified;
    private string $url;
    private Membership $membership;
    private bool $admin;
    private bool $owner;

    private ?ActivityTypeIcon $activityTypesIcon;

    /** @var ActivityType[] */
    private iterable $activityTypes;

    /** @var Dimension[] */
    private iterable $dimensions;

    public function __construct(array $club)
    {
        $this->setId($club['id']);
        $this->setResourceState(ResourceState::tryFrom($club['resource_state']));
        $this->setName($club['name']);
        $this->setProfile($club['profile']);
        $this->setProfileMedium($club['profile_medium']);
        $this->setSportType(SportType::tryFrom($club['sport_type']));
        $this->setCoverPhoto($club['cover_photo']);
        $this->setCoverPhotoSmall($club['cover_photo_small']);

        $this->setCity($club['city']);
        $this->setState($club['state']);
        $this->setCountry($club['country']);
        $this->setPrivate($club['private']);
        $this->setMemberCount($club['member_count']);
        $this->setFeatured($club['featured']);
        $this->setVerified($club['verified']);
        $this->setUrl($club['url']);
        $this->setMembership(Membership::tryFrom($club['membership']));
        $this->setAdmin($club['admin']);
        $this->setOwner($club['owner']);

        foreach ($club['activity_types'] as $activityType) {
            $this->addActivityType(ActivityType::tryFrom($activityType));
        }

        foreach ($club['dimensions'] as $dimension) {
            $this->addDimension(Dimension::tryFrom($dimension));
        }

        if ($this->getResourceState() === ResourceState::DETAIL) {
            $this->setActivityTypesIcon(ActivityTypeIcon::tryFrom($club['activity_types_icon']));
        }
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Club
     */
    public function setId(int $id): Club
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return ResourceState
     */
    public function getResourceState(): ResourceState
    {
        return $this->resourceState;
    }

    /**
     * @param ResourceState $resourceState
     * @return Club
     */
    public function setResourceState(ResourceState $resourceState): Club
    {
        $this->resourceState = $resourceState;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Club
     */
    public function setName(string $name): Club
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getProfileMedium(): string
    {
        return $this->profileMedium;
    }

    /**
     * @param string $profileMedium
     * @return Club
     */
    public function setProfileMedium(string $profileMedium): Club
    {
        $this->profileMedium = $profileMedium;

        return $this;
    }

    /**
     * @return string
     */
    public function getCoverPhoto(): ?string
    {
        return $this->coverPhoto;
    }

    /**
     * @param ?string $coverPhoto
     * @return Club
     */
    public function setCoverPhoto(?string $coverPhoto): Club
    {
        $this->coverPhoto = $coverPhoto;

        return $this;
    }

    public function getCoverPhotoSmall(): ?string
    {
        return $this->coverPhotoSmall;
    }

    public function setCoverPhotoSmall(?string $coverPhotoSmall): Club
    {
        $this->coverPhotoSmall = $coverPhotoSmall;

        return $this;
    }

    /**
     * @return SportType
     */
    public function getSportType(): SportType
    {
        return $this->sportType;
    }

    /**
     * @param SportType $sportType
     * @return Club
     */
    public function setSportType(SportType $sportType): Club
    {
        $this->sportType = $sportType;

        return $this;
    }

    /**
     * @return ActivityType
     */
    public function getActivityType(): ActivityType
    {
        return $this->activityType;
    }

    /**
     * @param ActivityType $activityType
     * @return Club
     */
    public function setActivityType(ActivityType $activityType): Club
    {
        $this->activityType = $activityType;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     * @return Club
     */
    public function setCity(string $city): Club
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     * @return Club
     */
    public function setState(string $state): Club
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPrivate(): bool
    {
        return $this->private;
    }

    /**
     * @param bool $private
     * @return Club
     */
    public function setPrivate(bool $private): Club
    {
        $this->private = $private;

        return $this;
    }

    /**
     * @return int
     */
    public function getMemberCount(): int
    {
        return $this->memberCount;
    }

    /**
     * @param int $memberCount
     * @return Club
     */
    public function setMemberCount(int $memberCount): Club
    {
        $this->memberCount = $memberCount;

        return $this;
    }

    /**
     * @return bool
     */
    public function isFeatured(): bool
    {
        return $this->featured;
    }

    /**
     * @param bool $featured
     * @return Club
     */
    public function setFeatured(bool $featured): Club
    {
        $this->featured = $featured;

        return $this;
    }

    /**
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->verified;
    }

    /**
     * @param bool $verified
     * @return Club
     */
    public function setVerified(bool $verified): Club
    {
        $this->verified = $verified;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Club
     */
    public function setUrl(string $url): Club
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return Membership
     */
    public function getMembership(): Membership
    {
        return $this->membership;
    }

    /**
     * @param Membership $membership
     * @return Club
     */
    public function setMembership(Membership $membership): Club
    {
        $this->membership = $membership;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->admin;
    }

    /**
     * @param bool $admin
     * @return Club
     */
    public function setAdmin(bool $admin): Club
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * @return bool
     */
    public function isOwner(): bool
    {
        return $this->owner;
    }

    /**
     * @param bool $owner
     * @return Club
     */
    public function setOwner(bool $owner): Club
    {
        $this->owner = $owner;

        return $this;
    }

    public function getProfile(): string
    {
        return $this->profile;
    }

    public function setProfile(string $profile): Club
    {
        $this->profile = $profile;

        return $this;
    }

    public function getActivityTypesIcon(): ActivityTypeIcon
    {
        return $this->activityTypesIcon;
    }

    public function setActivityTypesIcon(ActivityTypeIcon $activityTypesIcon): Club
    {
        $this->activityTypesIcon = $activityTypesIcon;

        return $this;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): Club
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return ActivityType[]
     */
    public function getActivityTypes(): iterable
    {
        return $this->activityTypes;
    }

    public function addActivityType(ActivityType $activityType): Club
    {
        $this->activityTypes[] = $activityType;

        return $this;
    }

    /**
     * @return Dimension[]
     */
    public function getDimensions(): iterable
    {
        return $this->dimensions;
    }

    public function addDimension(Dimension $dimension): Club
    {
        $this->dimensions[] = $dimension;

        return $this;
    }
}
