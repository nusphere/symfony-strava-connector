<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject;

use DateTime;
use DateTimeInterface;
use Exception;
use NuBox\Strava\Api\DataObject\Enum\ResourceState;

final class Subscription
{
    private int $id;
    private int $applicationId;
    private string $callbackUrl;
    private ResourceState $resourceState;
    private DateTimeInterface $createdAt;
    private DateTimeInterface $updatedAt;

    /**
     * @param array{
     *     id: int,
     *     resource_state: int,
     *     application_id: int,
     *     callback_url: string,
     *     created_at: string,
     *     updated_at: string,
     * } $subscription
     */
    public function __construct(array $subscription)
    {
        $this->setId($subscription['id']);
        $this->setResourceState(ResourceState::from($subscription['resource_state']));
        $this->setApplicationId($subscription['application_id']);
        $this->setCallbackUrl($subscription['callback_url']);

        try {
            $this->setCreatedAt(new DateTime($subscription['created_at']));
            $this->setUpdatedAt(new DateTime($subscription['updated_at']));
        } catch (Exception) {
            $this->setCreatedAt(new DateTime());
            $this->setUpdatedAt(new DateTime());
        }
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getResourceState(): ResourceState
    {
        return $this->resourceState;
    }

    public function setResourceState(ResourceState $resourceState): void
    {
        $this->resourceState = $resourceState;
    }

    public function getApplicationId(): int
    {
        return $this->applicationId;
    }

    public function setApplicationId(int $applicationId): void
    {
        $this->applicationId = $applicationId;
    }

    public function getCallbackUrl(): string
    {
        return $this->callbackUrl;
    }

    public function setCallbackUrl(string $callbackUrl): void
    {
        $this->callbackUrl = $callbackUrl;
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
