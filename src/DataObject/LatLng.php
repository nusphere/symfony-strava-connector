<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject;

final class LatLng
{
    private ?float $latitude;
    private ?float $longitude;

    public function __construct(array $latLngArray)
    {
        if (array_key_exists(0, $latLngArray)) {
            $this->setLatitude($latLngArray[0]);
        }

        if (array_key_exists(1, $latLngArray)) {
            $this->setLongitude($latLngArray[1] ?? null);
        }
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }
}
