<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject;

final class Segment
{
    private ?float $distance;
    private ?int $elapsedTime;
    private ?float $elevationDifference;
    private ?int $movingTime;
    private ?int $split;
    private ?float $averageSpeed;
    private ?float $averageGradeAdjustedSpeed;
    private ?float $averageHeartRate;
    private ?int $paceZone;

    public function __construct(array $segmentArray)
    {
        $this->setDistance($segmentArray['distance']);
        $this->setElapsedTime($segmentArray['elapsed_time']);
        $this->setElevationDifference($segmentArray['elevation_difference']);
        $this->setMovingTime($segmentArray['moving_time']);
        $this->setSplit($segmentArray['split']);
        $this->setAverageSpeed($segmentArray['average_speed']);
        $this->setAverageGradeAdjustedSpeed($segmentArray['average_grade_adjusted_speed']);
        $this->setAverageHeartRate($segmentArray['average_heartrate']);
        $this->setPaceZone($segmentArray['pace_zone']);
    }

    public function getDistance(): ?float
    {
        return $this->distance;
    }

    public function setDistance(float $distance): Segment
    {
        $this->distance = $distance;

        return $this;
    }

    public function getElapsedTime(): ?int
    {
        return $this->elapsedTime;
    }

    public function setElapsedTime(int $elapsedTime): Segment
    {
        $this->elapsedTime = $elapsedTime;

        return $this;
    }

    public function getElevationDifference(): ?float
    {
        return $this->elevationDifference;
    }

    public function setElevationDifference(float $elevationDifference): Segment
    {
        $this->elevationDifference = $elevationDifference;

        return $this;
    }

    public function getMovingTime(): ?int
    {
        return $this->movingTime;
    }

    public function setMovingTime(int $movingTime): Segment
    {
        $this->movingTime = $movingTime;

        return $this;
    }

    public function getSplit(): ?int
    {
        return $this->split;
    }

    public function setSplit(int $split): Segment
    {
        $this->split = $split;

        return $this;
    }

    public function getAverageSpeed(): ?float
    {
        return $this->averageSpeed;
    }

    public function setAverageSpeed(float $averageSpeed): Segment
    {
        $this->averageSpeed = $averageSpeed;

        return $this;
    }

    public function getAverageGradeAdjustedSpeed(): ?float
    {
        return $this->averageGradeAdjustedSpeed;
    }

    public function setAverageGradeAdjustedSpeed(?float $averageGradeAdjustedSpeed): Segment
    {
        $this->averageGradeAdjustedSpeed = $averageGradeAdjustedSpeed;

        return $this;
    }

    public function getAverageHeartRate(): ?float
    {
        return $this->averageHeartRate;
    }

    public function setAverageHeartRate(float $averageHeartRate): Segment
    {
        $this->averageHeartRate = $averageHeartRate;

        return $this;
    }

    public function getPaceZone(): ?int
    {
        return $this->paceZone;
    }

    public function setPaceZone(int $paceZone): Segment
    {
        $this->paceZone = $paceZone;

        return $this;
    }
}
