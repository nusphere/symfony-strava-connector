<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject;

use NuBox\Strava\Api\DataObject\Enum\GearType;
use NuBox\Strava\Api\DataObject\Enum\ResourceState;

/**
 * @phpstan-type GearArray array{
 *     id: string,
 *     resource_state: int,
 *     primary?: bool,
 *     name?: string,
 *     distance?: float,
 *     brand_name?: string,
 *     model_name?: string,
 *     frame_type?: int,
 *     description?: string,
 * }
 */
final class Gear
{
    // Meta info
    private string $id;
    private ResourceState $resourceState;
    private GearType $gearType;

    // Summary info
    private ?bool $primary;
    private ?string $name;
    private ?string $nickname;
    private ?bool $retired;
    private ?int $distance;
    private ?float $convertedDistance;

    // Detail info
    private ?string $brandName;
    private ?string $modelName;
    private ?int $frameType;
    private ?string $description;

    /**
     * @param GearArray $gear
     */
    public function __construct(array $gear)
    {
        $this->setId($gear['id']);
        $this->setResourceState(ResourceState::tryFrom($gear['resource_state']));
        $this->setGearType(GearType::tryFrom($this->getId()[0]));

        if ($this->getResourceState()->value >= ResourceState::SUMMARY->value) {
            $this->setPrimary($gear['primary']);
            $this->setName($gear['name']);
            $this->setNickname($gear['nickname']);
            $this->setDistance($gear['distance']);
            $this->setConvertedDistance($gear['converted_distance']);
            $this->setRetired($gear['retired']);

            if ($this->getResourceState() === ResourceState::DETAIL) {
                $this->setBrandName($gear['brand_name']);
                $this->setModelName($gear['model_name']);
                $this->setFrameType($gear['frame_type']);
                $this->setDescription($gear['description']);
            }
        }
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): Gear
    {
        $this->id = $id;

        return $this;
    }

    public function getResourceState(): ResourceState
    {
        return $this->resourceState;
    }

    public function setResourceState(ResourceState $resourceState): Gear
    {
        $this->resourceState = $resourceState;

        return $this;
    }

    public function isPrimary(): ?bool
    {
        return $this->primary;
    }

    public function setPrimary(bool $primary): Gear
    {
        $this->primary = $primary;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): Gear
    {
        $this->name = $name;

        return $this;
    }

    public function getDistance(): ?int
    {
        return $this->distance;
    }

    public function setDistance(int $distance): Gear
    {
        $this->distance = $distance;

        return $this;
    }

    public function getBrandName(): ?string
    {
        return $this->brandName;
    }

    public function setBrandName(string $brandName): Gear
    {
        $this->brandName = $brandName;

        return $this;
    }

    public function getModelName(): ?string
    {
        return $this->modelName;
    }

    public function setModelName(string $modelName): Gear
    {
        $this->modelName = $modelName;

        return $this;
    }

    public function getFrameType(): ?int
    {
        return $this->frameType;
    }

    public function setFrameType(int $frameType): Gear
    {
        $this->frameType = $frameType;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): Gear
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getNickname(): ?string
    {
        return $this->nickname;
    }

    /**
     * @param string $nickname
     * @return Gear
     */
    public function setNickname(?string $nickname): Gear
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRetired(): ?bool
    {
        return $this->retired;
    }

    /**
     * @param bool $retired
     * @return Gear
     */
    public function setRetired(bool $retired): Gear
    {
        $this->retired = $retired;

        return $this;
    }

    /**
     * @return float
     */
    public function getConvertedDistance(): ?float
    {
        return $this->convertedDistance;
    }

    /**
     * @param float $convertedDistance
     * @return Gear
     */
    public function setConvertedDistance(float $convertedDistance): Gear
    {
        $this->convertedDistance = $convertedDistance;

        return $this;
    }

    /**
     * @return GearType
     */
    public function getGearType(): GearType
    {
        return $this->gearType;
    }

    /**
     * @param GearType $gearType
     */
    public function setGearType(GearType $gearType): void
    {
        $this->gearType = $gearType;
    }

}
