<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DataObject;

use DateTimeInterface;

interface StravaTokenInterface
{
    public function getExpiresAt(): DateTimeInterface;
    public function setExpiresAt(DateTimeInterface $expireAt): self;

    public function getRefreshToken(): string;
    public function setRefreshToken(string $refreshToken): self;

    public function getTokenType(): string;
    public function setTokenType(string $tokenType): self;

    public function getAccessToken(): string;
    public function setAccessToken(string $accessToken): self;

    public function getScopes(): string;
    public function setScopes(string $scopes): self;

    public function isAccessTokenValid(): bool;
}
