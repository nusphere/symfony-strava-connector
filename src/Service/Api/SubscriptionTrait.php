<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Service\Api;

use JsonException;
use NuBox\Strava\Api\DataObject\Subscription;
use NuBox\Strava\Api\Exception\StravaApiRuntimeException;
use NuBox\Strava\Api\Exception\SubscriptionException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;

trait SubscriptionTrait
{
    final public function hasWebhookSubscription(string $callbackUrl): bool
    {
        $this->getLogger()->info('checking for callback url', [
            'callbackUrl' => $callbackUrl
        ]);

        foreach ($this->viewSubscriptions() as $subscription) {
            if ($subscription->getCallbackUrl() === $callbackUrl) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return Subscription[]
     */
    final public function viewSubscriptions(): iterable
    {
        try {
            $response = $this->getClient()->request(
                'GET',
                'https://www.strava.com/api/v3/push_subscriptions',
                [
                    'query' => [
                        'client_id' => $this->getStravaClientId(),
                        'client_secret' => $this->getStravaClientSecret()
                    ]
                ]
            );

            $subscriptions = $response->toArray();

            foreach ($subscriptions as $subscription) {
                yield new Subscription($subscription);
            }
        } catch (ExceptionInterface $exception) {
            throw new StravaApiRuntimeException(message: 'unable to load subscriptions', previous: $exception);
        }
    }

    /**
     * @throws SubscriptionException
     */
    final public function createSubscription(string $callBackUrl): bool
    {
        try {
            $response = $this->getClient()->request(
                'POST',
                'https://www.strava.com/api/v3/push_subscriptions',
                [
                    'query' => [
                        'client_id' => $this->getStravaClientId(),
                        'client_secret' => $this->getStravaClientSecret(),
                        'callback_url' => $callBackUrl,
                        'verify_token' => $this->getAppToken(),
                    ]
                ]
            );

            if ($response->getStatusCode() === Response::HTTP_CREATED) {
                return true;
            }

            $this->getLogger()->error('subscription not created', $response->toArray());

            return false;
        } catch (ExceptionInterface $exception) {
            throw new SubscriptionException(message: 'unable to create a subscription', previous: $exception);
        }
    }

    /**
     * @throws SubscriptionException
     */
    final public function webHookSubscription(Request $request): string
    {
        $this->getLogger()->info('subscription test occured', $request->query->all());

        // Validate
        $challenge    = $request->query->get('hub_challenge') ?? throw new SubscriptionException('Challenge missing');
        $mode         = $request->query->get('hub_mode') ?? throw new SubscriptionException('mode missing');
        $verify_token = $request->query->get('hub_verify_token') ?? throw new SubscriptionException(
            'verify_token missing'
        );

        if ($mode !== 'subscribe') {
            throw new SubscriptionException('Wrong mode');
        }

        if ($verify_token !== $this->getAppToken()) {
            throw new SubscriptionException('Wrong verify Token');
        }

        try {
            return json_encode(['hub.challenge' => $challenge], JSON_THROW_ON_ERROR);
        } catch (JsonException $jsonException) {
            throw new SubscriptionException(message: 'unable to setup challenge', previous: $jsonException);
        }
    }

    /**
     * @throws SubscriptionException
     */
    final public function deleteSubscription(string $callbackUrl): bool
    {
        try {
            foreach ($this->viewSubscriptions() as $subscription) {
                if ($subscription->getCallbackUrl() === $callbackUrl) {
                    $response = $this->getClient()->request(
                        'DELETE',
                        'https://www.strava.com/api/v3/push_subscriptions/' . $subscription->getId(),
                        [
                            'query' => [
                                'client_id' => $this->getStravaClientId(),
                                'client_secret' => $this->getStravaClientSecret(),
                            ]
                        ]
                    );

                    return $response->getStatusCode() === Response::HTTP_NO_CONTENT;
                }
            }

            return false;
        } catch (ExceptionInterface $exception) {
            throw new SubscriptionException(
                message: 'deleting subscription [' . $callbackUrl . '] not successfully',
                previous: $exception
            );
        }
    }
}
