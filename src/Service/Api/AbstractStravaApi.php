<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Service\Api;

use Psr\Log\LoggerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class AbstractStravaApi
{
    public function __construct(
        private HttpClientInterface $client,
        private readonly EventDispatcherInterface $dispatcher,
        private readonly LoggerInterface $logger,
        private readonly string $clientId,
        private readonly string $clientSecret,
        private readonly string $scope,
        private readonly bool $debugTracking = false,
        private readonly string $debugPath = '',
    ) {
    }

    final public function getStravaClientId(): int
    {
        return is_numeric($this->clientId) ? (int) $this->clientId : 0;
    }

    final public function getStravaClientSecret(): string
    {
        return $this->clientSecret;
    }

    final public function getScope(): string
    {
        return $this->scope;
    }

    /**
     * Returns an app token for the API
     *
     * @noinspection MethodShouldBeFinalInspection
     */
    public function getAppToken(): string
    {
        return 'nubox-strava-api';
    }

    final public function getDispatcher(): EventDispatcherInterface
    {
        return $this->dispatcher;
    }

    final public function getClient(): HttpClientInterface
    {
        return $this->client;
    }

    final public function setClient(HttpClientInterface $client): self
    {
        $this->client = $client;

        return $this;
    }

    final public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    final public function isDebugTracking(): bool
    {
        return $this->debugTracking;
    }

    final public function getDebugPath(): string
    {
        return $this->debugPath;
    }

    final public function debugDump(string $filename, string $json): void
    {
        if ($this->isDebugTracking()) {
            file_put_contents($this->getDebugPath() . DIRECTORY_SEPARATOR . $filename . '.json', $json);
        }
    }
}
