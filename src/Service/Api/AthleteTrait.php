<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Service\Api;

use NuBox\Strava\Api\DataObject\Athlete;
use NuBox\Strava\Api\DataObject\StravaTokenInterface;
use NuBox\Strava\Api\Exception\AthleteException;
use NuBox\Strava\Api\Exception\TokenException;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;

trait AthleteTrait
{
    /**
     * @throws TokenException
     * @throws AthleteException
     */
    final public function getAthlete(StravaTokenInterface $token): Athlete
    {
        if (!$token->isAccessTokenValid()) {
            $token = $this->refreshToken($token);
        }

        try {
            $response = $this->getClient()->request(
                'GET',
                'https://www.strava.com/api/v3/athlete',
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token->getAccessToken(),
                    ]
                ]
            );

            $athleteJson = $response->getContent();

            $this->debugDump('athlete' . time(), $athleteJson);

            $athleteArray = json_decode($athleteJson, true);

            return new Athlete($athleteArray);
        } catch (ExceptionInterface $exception) {
            throw new AthleteException(message: 'unable to load athlete', previous: $exception);
        }
    }
}
