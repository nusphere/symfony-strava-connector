<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Service\Api;

use DateTime;
use Exception;
use NuBox\Strava\Api\DataObject\Athlete;
use NuBox\Strava\Api\DataObject\StravaToken;
use NuBox\Strava\Api\DataObject\StravaTokenInterface;
use NuBox\Strava\Api\Event\StravaApiEvents;
use NuBox\Strava\Api\Event\Token\CreatedEvent;
use NuBox\Strava\Api\Event\Token\InvalidEvent;
use NuBox\Strava\Api\Event\Token\RefreshedEvent;
use NuBox\Strava\Api\Event\Token\RequestEvent;
use NuBox\Strava\Api\Event\Token\RevokedEvent;
use NuBox\Strava\Api\Exception\MissingTokenException;
use NuBox\Strava\Api\Exception\RevokeException;
use NuBox\Strava\Api\Exception\TokenException;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;

trait TokenTrait
{
    final public function getAuthorizationUrl(string $redirectUri): string
    {
        return 'https://www.strava.com/oauth/authorize?' . http_build_query([
                'client_id' => $this->getStravaClientId(),
                'response_type' => 'code',
                'redirect_uri' => $redirectUri,
                'approval_prompt' => false,
                'scope' => $this->getScope()
            ]);
    }

    /**
     * @throws TokenException
     */
    final public function authorizationCode(string $authCode, string $scope): StravaToken
    {
        try {
            $response = $this->getClient()->request(
                'POST',
                'https://www.strava.com/api/v3/oauth/token',
                [
                    'query' => [
                        'client_id' => $this->getStravaClientId(),
                        'client_secret' => $this->getStravaClientSecret(),
                        'code' => $authCode,
                        'grant_type' => 'authorization_code',
                    ]
                ]
            );

            $tokenInfo = $response->toArray();

            $stravaToken = new StravaToken();
            $stravaToken->setAthlete(new Athlete($tokenInfo['athlete']));
            $stravaToken->setScopes($scope);

            $this->updateToken($stravaToken, $tokenInfo);

            // Let the user create the token
            $this->getDispatcher()->dispatch(new CreatedEvent($stravaToken), StravaApiEvents::TOKEN_CREATED);

            return $stravaToken;
        } catch (ExceptionInterface | Exception $exception) {
            throw new TokenException(message: 'token generation failed', previous: $exception);
        }
    }

    /**
     * @throws TokenException
     */
    final public function refreshToken(StravaTokenInterface $token): StravaTokenInterface
    {
        $originalToken = $token;

        try {
            $response = $this->getClient()->request(
                'POST',
                'https://www.strava.com/api/v3/oauth/token',
                [
                    'query' => [
                        'client_id' => $this->getStravaClientId(),
                        'client_secret' => $this->getStravaClientSecret(),
                        'refresh_token' => $token->getRefreshToken(),
                        'grant_type' => 'refresh_token',
                    ]
                ]
            );

            $this->updateToken($token, $response->toArray());
        } catch (ExceptionInterface | Exception $exception) {
            throw new TokenException(message: 'refresh not possible', previous: $exception);
        }

        // Let the user update the token
        $this->getDispatcher()->dispatch(
            new RefreshedEvent(token: $token, originalToken: $originalToken),
            StravaApiEvents::TOKEN_REFRESHED
        );

        return $token;
    }

    /**
     * @throws MissingTokenException
     * @throws RevokeException
     */
    final public function revoke(?StravaTokenInterface $token): bool
    {
        try {
            if ($token === null) {
                $event = new RequestEvent();

                $this->getDispatcher()->dispatch($event, StravaApiEvents::TOKEN_REQUEST);
                $token = $event->getToken();
            }

            if (!$token instanceof StravaTokenInterface) {
                throw new MissingTokenException('no token provided');
            }

            if (!$token->isAccessTokenValid()) {
                $token = $this->refreshToken($token);
            }

            $this->getClient()->request(
                'POST',
                'https://www.strava.com/oauth/deauthorize',
                [
                    'query' => [
                        'access_token' => $token->getAccessToken(),
                    ]
                ]
            );

            $this->getDispatcher()->dispatch(new RevokedEvent($token), StravaApiEvents::TOKEN_REVOKED);
        } catch (TokenException) {
            $this->getDispatcher()->dispatch(new InvalidEvent($token), StravaApiEvents::TOKEN_INVALID);
        } catch (ExceptionInterface $exception) {
            throw new RevokeException(message: 'revoke is not possible', previous: $exception);
        }

        return true;
    }

    /**
     * @throws Exception
     */
    final public function updateToken(StravaTokenInterface $token, array $tokenInfo): void
    {
        $token->setTokenType($tokenInfo['token_type']);
        $token->setExpiresAt(new DateTime('@'.$tokenInfo['expires_at']));
        $token->setExpiresIn(new DateTime('@'.($tokenInfo['expires_in'] + time())));
        $token->setRefreshToken($tokenInfo['refresh_token']);
        $token->setAccessToken($tokenInfo['access_token']);
    }
}
