<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Service\Api;

use NuBox\Strava\Api\DataObject\Activity;
use NuBox\Strava\Api\DataObject\StravaTokenInterface;
use NuBox\Strava\Api\Exception\ActivityException;
use NuBox\Strava\Api\Exception\TokenException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ExceptionInterface;

trait ActivityTrait
{
    /**
     * @throws TokenException
     * @throws ActivityException
     */
    final public function getActivity(StravaTokenInterface $token, int $activityId): Activity
    {
        if (!$token->isAccessTokenValid()) {
            $token = $this->refreshToken($token);
        }

        try {
            $response = $this->getClient()->request(
                'GET',
                'https://www.strava.com/api/v3/activities/' . $activityId,
                [
                    'headers' => [
                        'Authorization' => 'Bearer ' . $token->getAccessToken(),
                    ],
                    'query' => [
                        'include_all_efforts' => true,
                    ]
                ]
            );

            $activityJson = $response->getContent();

            $athleteArray = json_decode($activityJson, true);

            $this->debugDump('activity_' . $activityId, $activityJson);

            return new Activity($athleteArray);
        } catch (ExceptionInterface $exception) {
            throw new ActivityException(message: 'unable to load activity', previous: $exception);
        }
    }

    public function updateActivity(StravaTokenInterface $token, mixed $stravaId, array $options)
    {
        $response = $this->getClient()->request(
            'PUT',
            'https://www.strava.com/api/v3/activities/' . $stravaId,
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token->getAccessToken(),
                ],
                'query' => $options
            ]
        );

        return json_decode($response->getContent(), true);
    }

    public function createActivity(StravaTokenInterface $token, array $options)
    {
        $response = $this->getClient()->request(
            'POST',
            'https://www.strava.com/api/v3/activities',
            [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token->getAccessToken(),
                ],
                'query' => $options
            ]
        );

        return json_decode($response->getContent(), true);
    }

    public function createUpload(StravaTokenInterface $token, array $options, bool $compressed = false): int
    {
        if ($compressed) {
            $options['file'] = gzcompress($options['file']);
            $options['data_type'] .= '.gz';
        }

        $file_name = '/tmp/test_' . time() . '.' . $options['data_type'];

        file_put_contents($file_name, $options['file']);

        $options['file'] = DataPart::fromPath($file_name);

        $formData = new FormDataPart($options);

        $response = $this->getClient()->request(
            'POST',
            'https://www.strava.com/api/v3/uploads',
            [
                'auth_bearer' => $token->getAccessToken(),
                'headers' => $formData->getPreparedHeaders()->toArray(),
                'body' => $formData->bodyToIterable(),
            ]
        );

        $response = $response->toArray();

        do {
            sleep(2);

            $uploadResponse = $this->client->request(
                'GET',
                'https://www.strava.com/api/v3/uploads/' . $response['id'],
                [
                    'auth_bearer' => $token->getAccessToken(),
                ]
            );

            $uploadInfo = $uploadResponse->toArray();

            if ($uploadInfo['activity_id'] !== null) {
                return $uploadInfo['activity_id'];
            }

            if ($uploadInfo['error'] !== null) {
                if (str_contains($uploadInfo['error'], 'duplicate of activity')) {
                    $splittedText = explode(' ', $uploadInfo['error']);

                    return (int) $splittedText[4];
                }

                throw new RuntimeException($uploadInfo['error']);
            }

        } while ($uploadResponse->getStatus() === Response::HTTP_OK);
    }
}
