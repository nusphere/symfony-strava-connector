<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Service;

use NuBox\Strava\Api\Service\Api\AbstractStravaApi;
use NuBox\Strava\Api\Service\Api\ActivityTrait;
use NuBox\Strava\Api\Service\Api\AthleteTrait;
use NuBox\Strava\Api\Service\Api\SubscriptionTrait;
use NuBox\Strava\Api\Service\Api\TokenTrait;

final class StravaApi extends AbstractStravaApi
{
    use AthleteTrait;
    use TokenTrait;
    use ActivityTrait;
    use SubscriptionTrait;

    public const TYPE = 'strava';
}
