<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Exception;

use Exception;

class RevokeException extends Exception
{
}
