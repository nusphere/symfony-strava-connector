<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Exception;

use RuntimeException;

class StravaApiRuntimeException extends RuntimeException
{
}
