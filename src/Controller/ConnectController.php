<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Controller;

use NuBox\Strava\Api\DataObject\StravaTokenInterface;
use NuBox\Strava\Api\Exception\MissingTokenException;
use NuBox\Strava\Api\Exception\RevokeException;
use NuBox\Strava\Api\Service\StravaApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

final class ConnectController extends AbstractController
{
    public function __construct(
        private readonly StravaApi $stravaApi,
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly string $redirectRoute,
    ) {
    }

    #[Route('/connect', 'nubox.strava_api.connect')]
    public function connect(): Response
    {
        // ask the router for the correct url
        $redirectUrl = $this
            ->urlGenerator
            ->generate('nubox.strava_api.connect.token', [], UrlGeneratorInterface::ABSOLUTE_URL);

        return $this->redirect($this->stravaApi->getAuthorizationUrl($redirectUrl));
    }

    #[Route('/connect/token', 'nubox.strava_api.connect.token')]
    public function validateAuthorizationCode(Request $request): Response
    {
        if ($request->get('error') !== null) {
            $this->addFlash(
                'error',
                'an error occurred'
            );

            return $this->redirectToRoute($this->redirectRoute);
        }

        // Get StravaToken by AuthCode
        $this->stravaApi->authorizationCode(
            $request->get('code'),
            $request->get('scope'),
        );

        return $this->redirectToRoute($this->redirectRoute);
    }

    /**
     * @throws MissingTokenException
     * @throws RevokeException
     */
    #[Route('/connect/revoke', 'nubox.strava_api.connect.revoke')]
    public function revoke(Request $request): Response
    {
        $socialToken = $request->get('token');

        if (is_null($socialToken) || $socialToken instanceof StravaTokenInterface) {
            $this->stravaApi->revoke($socialToken);
        } else {
            throw new MissingTokenException(
                'the provided token via GET is no instance of StravaTokenInterface'
            );
        }

        return $this->redirectToRoute($this->redirectRoute);
    }
}
