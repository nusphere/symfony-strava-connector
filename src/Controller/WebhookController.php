<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Controller;

use JsonException;
use NuBox\Strava\Api\DataObject\Enum\AspectType;
use NuBox\Strava\Api\DataObject\Enum\ObjectType;
use NuBox\Strava\Api\DataObject\Webhook;
use NuBox\Strava\Api\Event\Webhook\ActivityCreatedEvent;
use NuBox\Strava\Api\Event\Webhook\ActivityDeletedEvent;
use NuBox\Strava\Api\Event\Webhook\ActivityUpdatedEvent;
use NuBox\Strava\Api\Event\Webhook\AthleteCreatedEvent;
use NuBox\Strava\Api\Event\Webhook\AthleteDeletedEvent;
use NuBox\Strava\Api\Event\Webhook\AthleteUpdatedEvent;
use NuBox\Strava\Api\Exception\SubscriptionException;
use NuBox\Strava\Api\Exception\WebhookException;
use NuBox\Strava\Api\Service\StravaApi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

final class WebhookController extends AbstractController
{
    public function __construct(
        private readonly StravaApi $stravaApi,
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly EventDispatcherInterface $dispatcher,
    ) {
    }

    /**
     * @throws WebhookException
     * @throws SubscriptionException
     */
    #[Route('/webhook', 'nubox.strava_api.webhook')]
    public function webhookEndpoint(Request $request): Response
    {
        if ($request->getMethod() === Request::METHOD_GET) {
            return new Response($this->stravaApi->webHookSubscription($request));
        }

        if (!str_starts_with($request->headers->get('Content-Type'), 'application/json')) {
            throw new WebhookException('only json allowed');
        }

        try {
            $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

            $webhook = new Webhook($data);

            $webhookEvent = match ($webhook->getObjectType()) {
                // Activity webhook aspect decision
                ObjectType::ACTIVITY => match ($webhook->getAspectType()) {
                    AspectType::UPDATE => new ActivityUpdatedEvent($webhook),
                    AspectType::CREATE => new ActivityCreatedEvent($webhook),
                    AspectType::DELETE => new ActivityDeletedEvent($webhook),
                },

                // Athlete webhook aspect decision
                ObjectType::ATHLETE => match ($webhook->getAspectType()) {
                    AspectType::UPDATE => new AthleteUpdatedEvent($webhook),
                    AspectType::CREATE => new AthleteCreatedEvent($webhook),
                    AspectType::DELETE => new AthleteDeletedEvent($webhook),
                },
            };

            $this->dispatcher->dispatch($webhookEvent, $webhookEvent::EVENT);

            return new Response(json_encode('done', JSON_THROW_ON_ERROR));
        } catch (JsonException $exception) {
            throw new WebhookException(message: 'error while webhook handling', previous: $exception);
        }
    }


    /**
     * @throws SubscriptionException
     */
    #[Route('/webhook/toggle', 'nubox.strava_api.webhook.toggle')]
    public function toggleWebhook(Request $request): Response
    {
        $callBackUrl = $this->urlGenerator->generate(
            'nubox.strava_api.webhook',
            [],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        if ($this->stravaApi->hasWebhookSubscription($callBackUrl)) {
            $this->stravaApi->deleteSubscription($callBackUrl);
        } else {
            $this->stravaApi->createSubscription($callBackUrl);
        }

        return new RedirectResponse(
            $request->headers->get('referer')
        );
    }
}
