<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Event\Token;

use NuBox\Strava\Api\DataObject\StravaTokenInterface;

interface TokenEventInterface
{
    public function getToken(): StravaTokenInterface;
}
