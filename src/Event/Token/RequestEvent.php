<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Event\Token;

use NuBox\Strava\Api\DataObject\StravaTokenInterface;
use Symfony\Contracts\EventDispatcher\Event;

final class RequestEvent extends Event implements TokenEventInterface
{
    public const EVENT = 'strava_api.token.request';

    private ?StravaTokenInterface $token;

    public function getToken(): StravaTokenInterface
    {
        assert($this->token instanceof StravaTokenInterface);

        return $this->token;
    }

    public function setToken(StravaTokenInterface $token): void
    {
        $this->token = $token;
    }
}
