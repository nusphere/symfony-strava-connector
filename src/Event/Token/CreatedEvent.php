<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Event\Token;

use NuBox\Strava\Api\DataObject\StravaToken;
use NuBox\Strava\Api\DataObject\StravaTokenInterface;

/**
 * This event is called when an oauth process is finished
 */
final class CreatedEvent implements TokenEventInterface
{
    public const EVENT = 'strava_api.token.created';

    public function __construct(
        private readonly StravaTokenInterface $token
    ) {
    }

    public function getToken(): StravaToken
    {
        return $this->token;
    }
}
