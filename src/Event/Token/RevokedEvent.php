<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Event\Token;

final class RevokedEvent extends TokenEvent
{
    public const EVENT = 'strava_api.token.revoked';
}
