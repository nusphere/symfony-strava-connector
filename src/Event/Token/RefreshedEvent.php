<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Event\Token;

use NuBox\Strava\Api\DataObject\StravaTokenInterface;

final class RefreshedEvent extends TokenEvent
{
    private StravaTokenInterface $originalToken;

    public const EVENT = 'strava_api.token.refreshed';

    public function __construct(StravaTokenInterface $token, StravaTokenInterface $originalToken)
    {
        $this->originalToken = $originalToken;

        parent::__construct($token);
    }

    public function getOriginalToken(): StravaTokenInterface
    {
        return $this->originalToken;
    }
}
