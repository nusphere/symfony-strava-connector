<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Event\Token;

use NuBox\Strava\Api\DataObject\StravaTokenInterface;
use Symfony\Contracts\EventDispatcher\Event;

abstract class TokenEvent extends Event implements TokenEventInterface
{
    public function __construct(
        protected readonly StravaTokenInterface $token
    ) {
    }

    final public function getToken(): StravaTokenInterface
    {
        return $this->token;
    }
}
