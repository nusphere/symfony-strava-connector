<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Event;

use NuBox\Strava\Api\Event\Token\CreatedEvent;
use NuBox\Strava\Api\Event\Token\InvalidEvent;
use NuBox\Strava\Api\Event\Token\RefreshedEvent;
use NuBox\Strava\Api\Event\Token\RequestEvent;
use NuBox\Strava\Api\Event\Token\RevokedEvent;
use NuBox\Strava\Api\Event\Webhook\ActivityCreatedEvent;
use NuBox\Strava\Api\Event\Webhook\ActivityDeletedEvent;
use NuBox\Strava\Api\Event\Webhook\ActivityUpdatedEvent;
use NuBox\Strava\Api\Event\Webhook\AthleteCreatedEvent;
use NuBox\Strava\Api\Event\Webhook\AthleteDeletedEvent;
use NuBox\Strava\Api\Event\Webhook\AthleteUpdatedEvent;

final class StravaApiEvents
{
    /**
     * Called when StravaToken is created and contains the StravaToken Object
     *
     * @Event("NuBox\Strava\Api\Event\Token\CreatedEvent")
     */
    public const TOKEN_CREATED = CreatedEvent::EVENT;

    /**
     * Called when StravaToken is revoked and contains the StravaToken Object
     *
     * @Event("NuBox\Strava\Api\Event\Token\RevokedEvent")
     */
    public const TOKEN_REVOKED = RevokedEvent::EVENT;

    /**
     * Called when StravaToken is refreshed and contains the StravaToken Object
     *
     * @Event("NuBox\Strava\Api\Event\Token\RefreshedEvent")
     */
    public const TOKEN_REFRESHED = RefreshedEvent::EVENT;

    /**
     * Called when StravaToken is refreshed and contains the StravaToken Object
     *
     * @Event("NuBox\Strava\Api\Event\Token\RequestEvent")
     */
    public const TOKEN_REQUEST = RequestEvent::EVENT;

    /**
     * Called when StravaToken is invalid and contains the invalid StravaToken Object
     *
     * @Event("NuBox\Strava\Api\Event\Token\InvalidEvent")
     */
    public const TOKEN_INVALID = InvalidEvent::EVENT;

    /**
     * Called when a webhook for an activity creation is called
     *
     * @Event("NuBox\Strava\Api\Event\Webhook\ActivityCreatedEvent")
     */
    public const WEBHOOK_ACTIVITY_CREATED = ActivityCreatedEvent::EVENT;

    /**
     * Called when a webhook for an activity update is called
     *
     * @Event("NuBox\Strava\Api\Event\Webhook\ActivityUpdatedEvent")
     */
    public const WEBHOOK_ACTIVITY_UPDATED = ActivityUpdatedEvent::EVENT;

    /**
     * Called when a webhook for an activity deletion is called
     *
     * @Event("NuBox\Strava\Api\Event\Webhook\ActivityDeletedEvent")
     */
    public const WEBHOOK_ACTIVITY_DELETED = ActivityDeletedEvent::EVENT;

    /**
     * Called when the user remove his oauth subscription
     *
     * @Event("NuBox\Strava\Api\Event\Webhook\AthleteDeletedEvent")
     */
    public const WEBHOOK_ATHLETE_DELETED = AthleteDeletedEvent::EVENT;
}
