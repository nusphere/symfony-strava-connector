<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Event\Webhook;

use NuBox\Strava\Api\DataObject\Webhook;

interface WebhookEventInterface
{
    public function getWebhook(): Webhook;
}
