<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Event\Webhook;

use NuBox\Strava\Api\DataObject\Webhook;
use Symfony\Contracts\EventDispatcher\Event;

abstract class WebhookEvent extends Event implements WebhookEventInterface
{
    public function __construct(private readonly Webhook $webhook)
    {
    }

    final public function getWebhook(): Webhook
    {
        return $this->webhook;
    }
}
