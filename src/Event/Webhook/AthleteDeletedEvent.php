<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Event\Webhook;

final class AthleteDeletedEvent extends WebhookEvent
{
    public const EVENT = 'strava_api.webhook.athlete.deleted';
}
