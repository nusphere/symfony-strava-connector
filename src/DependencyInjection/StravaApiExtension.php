<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DependencyInjection;

use Exception;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

final class StravaApiExtension extends Extension
{
    public const ALIAS = 'strava_api';

    /**
     * @param array<int, mixed> $configs
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(
            dirname(__DIR__, 2) . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR
        ));

        $loader->load('services.yaml');

        $configuration = $this->getConfiguration($configs, $container);

        assert($configuration instanceof ConfigurationInterface);

        $config = $this->processConfiguration($configuration, $configs);

        $definitionApiService = $container->getDefinition('nubox.strava_api.service.strava_api');
        $definitionApiService->setArgument(3, $config['client_id']);
        $definitionApiService->setArgument(4, $config['client_secret']);
        $definitionApiService->setArgument(5, $config['scope']);
        $definitionApiService->setArgument(6, $config['debug_tracking']);
        $definitionApiService->setArgument(7, $config['debug_path']);

        $definitionConnectController = $container->getDefinition('nubox.strava_api.controller.connect');
        $definitionConnectController->setArgument(2, $config['redirect_route']);
    }

    public function getAlias(): string
    {
        return self::ALIAS;
    }
}
