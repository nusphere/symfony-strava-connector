<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder(StravaApiExtension::ALIAS);
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
        ->children()
            ?->scalarNode('client_id')->info('description todo')->defaultValue('0')->end()
            ?->scalarNode('client_secret')->info('description todo')->defaultValue('')->end()
            ?->booleanNode('debug_tracking')->info('should the strava response be tracked as file?')->defaultValue(false)->end()
            ?->scalarNode('debug_path')->info('where should the tracking be safed')->defaultValue(sys_get_temp_dir())->end()
            ?->scalarNode('redirect_route')
                ->info('description todo')
                ->defaultValue('app_homepage')
            ->end()
            ?->scalarNode('scope')
                ->info('define your scopes for the oauth request')
                ->defaultValue('activity:read_all,activity:write,profile:read_all,profile:write,read_all,read')
            ->end()
        ?->end()
        ;

        return $treeBuilder;
    }
}
