<?php

declare(strict_types=1);

namespace NuBox\Strava\Api\Twig;

use Symfony\Bridge\Twig\Extension\AssetExtension;
use Symfony\Bridge\Twig\Extension\RoutingExtension;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class StravaButtonExtension extends AbstractExtension
{
    public function __construct(
        private readonly RoutingExtension $routing,
        private readonly AssetExtension $asset,
    ) {
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('strava_connect_button', [$this, 'stravaConnectButton'], ['is_safe' => ['html']]),
            new TwigFunction('strava_powered_image', [$this, 'stravaPoweredImage'], ['is_safe' => ['html']]),
            new TwigFunction('strava_revoke_button', [$this, 'stravaRevokeButton'], ['is_safe' => ['html']]),
        ];
    }

    public function stravaConnectButton(bool $lightButton = false): string
    {
        // ask the router for the correct url
        $redirectUrl = $this->routing->getPath('nubox.strava_api.connect');

        $stravaSVG = match ($lightButton) {
            true => $this->asset->getAssetUrl('/bundles/stravaapi/btn_strava_connectwith_light.svg'),
            false => $this->asset->getAssetUrl('/bundles/stravaapi/btn_strava_connectwith_orange.svg'),
        };

        return '<a href="' . $redirectUrl . '"><img src="' . $stravaSVG . '" /></a>';
    }

    public function stravaRevokeButton(): string
    {
        $redirectUrl = $this->routing->getPath('nubox.strava_api.revoke');
        $stravaSVG = $this->asset->getAssetUrl('/bundles/stravaapi/btn_strava_connectwith_orange.svg');

        return '<a href="' . $redirectUrl . '"><img src="' . $stravaSVG . '" /></a>';
    }

    public function stravaPoweredImage(string $type = 'default', string $orientation = 'horiz'): string
    {
        $stravaSVG = match ($type) {
            'light' => $this->asset->getAssetUrl('/bundles/stravaapi/api_logo_pwrdBy_strava_' . $orientation . '_light.svg'),
            'white' => $this->asset->getAssetUrl('/bundles/stravaapi/api_logo_pwrdBy_strava_' . $orientation . '_white.svg'),
            default => $this->asset->getAssetUrl('/bundles/stravaapi/api_logo_pwrdBy_strava_' . $orientation . '_gray.svg'),
        };

        return '<a href="https://www.strava.com"><img src="' . $stravaSVG . '" /></a>';
    }
}
